package com.jsrmi.ws;

import org.eclipse.jetty.websocket.server.JettyServerUpgradeRequest;
import org.eclipse.jetty.websocket.server.JettyServerUpgradeResponse;
import org.eclipse.jetty.websocket.server.JettyWebSocketCreator;

import java.util.List;
import java.util.Map;
import java.util.function.BiFunction;

public class RMIWebsocketCreator implements JettyWebSocketCreator {
	private BiFunction<RMISocket, Map<String, List<String>>, RMIEndpoint> wsCreator;


	@Override
	public Object createWebSocket(JettyServerUpgradeRequest req, JettyServerUpgradeResponse resp) {
		Map<String, List<String>> params = req.getParameterMap();
		RMISocket ws = new RMISocket( params);
		RMIEndpoint rmiEndpoint=wsCreator.apply(ws, params);
		ws.setRMIEndPoint(rmiEndpoint);
		return new JettyWebsocket(ws);
	}

	public void setWSCreator(BiFunction<RMISocket, Map<String, List<String>>, RMIEndpoint> wsCreator) {
		this.wsCreator=wsCreator;
	}
}
