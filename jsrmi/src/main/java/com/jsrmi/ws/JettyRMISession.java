package com.jsrmi.ws;

import org.eclipse.jetty.websocket.api.Session;

import java.io.IOException;
import java.nio.ByteBuffer;

public class JettyRMISession extends RMISession{
	private final Session session;

	public JettyRMISession(Session session){
		this.session=session;
	}
	@Override
	public void sendBytes(ByteBuffer buffer) throws IOException {
		session.getRemote().sendBytes(buffer);
	}

	@Override
	public void close() {
		session.close();
	}
}
