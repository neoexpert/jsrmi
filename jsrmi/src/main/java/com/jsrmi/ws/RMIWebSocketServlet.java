package com.jsrmi.ws;

import org.eclipse.jetty.websocket.server.JettyWebSocketServlet;
import org.eclipse.jetty.websocket.server.JettyWebSocketServletFactory;

import java.util.List;
import java.util.Map;
import java.util.function.BiFunction;

public class RMIWebSocketServlet extends JettyWebSocketServlet {
	private final RMIWebsocketCreator wsCreator;
	public RMIWebSocketServlet(){
		wsCreator=new RMIWebsocketCreator();
	}

	@Override
	protected void configure(JettyWebSocketServletFactory factory) {
		factory.addMapping("/", wsCreator);
	}

	public void setWsCreator(BiFunction<RMISocket, Map<String, List<String>>, RMIEndpoint> wsCreator) {
		this.wsCreator.setWSCreator(wsCreator);
	}
}
