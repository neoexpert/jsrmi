package com.jsrmi.ws;

import java.util.*;

import static com.jsrmi.ws.Types.*;

public class JSBuilder{
	private static class MethodType{
		final byte rtype;
		final byte[] ptypes;
		final int hashCode;
		public MethodType(byte rtype, byte[] ptypes){
			this.rtype=rtype;
			this.ptypes=ptypes;
			int hashCode=rtype+Arrays.hashCode(ptypes);
			this.hashCode=hashCode;
		}

		public boolean equals(Object o){
			MethodType other=(MethodType)o;
			return rtype==other.rtype&&Arrays.equals(ptypes, other.ptypes);
			
		}

		public int hashCode(){
			return hashCode;
		}
	}
	static HashMap<MethodType, String> heads=new HashMap<>();
	public static String getHead(byte rtype, byte[] ptypes){
		MethodType mt=new MethodType(rtype, ptypes);
		String js=heads.get(mt);
		if(js==null){
			js=buildHead(rtype, ptypes);
		}
		return js;	
	}

	static String s(byte type){
		switch(type){
			case VOID:
				return "V";
			case BOOLEAN:
				return "Z";
			case INT8:
				return "B";
			case INT16:
				return "S";
			case UINT16:
				return "C";
			case INT32:
				return "I";
			case INT64:
				return "J";
			case FLOAT32:
				return "F";
			case FLOAT64:
				return "D";
			case STRING:
				return "_S";
			case BOOLEAN_ARRAY:
				return "_ZA";
			case INT8_ARRAY:
				return "_ZA";
			case INT16_ARRAY:
				return "_SA";
			case UINT16_ARRAY:
				return "_CA";
			case INT32_ARRAY:
				return "_IA";
			case INT64_ARRAY:
				return "_JA";
			case FLOAT32_ARRAY:
				return "_FA";
			case FLOAT64_ARRAY:
				return "_DA";
			case STRING_ARRAY:
				return "__SA";
			default:
					throw new RuntimeException();
		}
	}
	
	private static String buildName(byte rtype, byte[] ptypes){
		StringBuilder sb=new StringBuilder();
		sb.append(s(rtype));
		for(byte b:ptypes){
			sb.append(s(b));
		}

		return sb.toString();
	}

	private static String buildHead(byte rtype, byte[] ptypes){
		StringBuilder sb=new StringBuilder();
		sb.append("async ");
		sb.append("function ");
		sb.append(buildName(rtype, ptypes));
		sb.append("(v, handler)");
		sb.append("{");
		StringBuilder isb=new StringBuilder();
		if(rtype!=VOID)
			isb.append("return ");
		isb.append("await handler(");
		int pos=0;
		String prefix="";
		int i=0;
loop:for(;i<ptypes.length; ++i){
			byte type=ptypes[i];
			isb.append(prefix);
			switch(type){
				case BOOLEAN:
					isb.append("v.getInt8(")
						.append(pos)
						.append(")===1");
					++pos;
					break;
				case INT8:
					isb.append("v.getInt8(")
						.append(pos)
						.append(")");
					++pos;
					break;
				case INT16:
					isb.append("v.getInt16(")
						.append(pos)
						.append(")");
					pos+=2;
					break;
				case UINT16:
					isb.append("v.getUint16(")
						.append(pos)
						.append(")");
					pos+=2;
					break;
				case INT32:
					isb.append("v.getInt32(")
						.append(pos)
						.append(")");
					pos+=4;
					break;
				case INT64:
					isb.append("v.getInt32(")
						.append(pos)
						.append(")");
					pos+=8;
					break;
				case FLOAT32:
					isb.append("v.getFloat32(")
						.append(pos)
						.append(")");
					pos+=4;
					break;
				case FLOAT64:
					isb.append("v.getFloat64(")
						.append(pos)
						.append(")");
					pos+=4;
					break;
				case STRING:
				case BOOLEAN_ARRAY:
				case INT8_ARRAY:
				case INT16_ARRAY:
				case UINT16_ARRAY:
				case INT32_ARRAY:
				case INT64_ARRAY:
				case FLOAT32_ARRAY:
				case FLOAT64_ARRAY:
				case STRING_ARRAY:
					break loop;
				default:
					throw new RuntimeException();
			}
			prefix=",";
		}
		if(i<ptypes.length){
			boolean lenDefined=false;
			sb.append("let pos=0;");
			for(;i<ptypes.length; ++i){
				byte type=ptypes[i];
				isb.append(prefix);
				prefix=",";
				isb
					.append("v")
					.append(i);
				switch(type){
					case BOOLEAN:
						sb
							.append("let v")
							.append(i)
							.append("=")
							.append("v.getInt8(")
							.append("pos++")
							.append(")===1;");
						break;
					case INT8:
						sb
							.append("let v")
							.append(i)
							.append("=")
							.append("v.getInt8(")
							.append("pos++")
							.append(");");
						break;
					case INT16:
						sb
							.append("let v")
							.append(i)
							.append("=")
							.append("v.getInt16(")
							.append("pos")
							.append(");")
							.append("pos+=2;");
						break;
					case UINT16:
						sb
							.append("let v")
							.append(i)
							.append("=")
							.append("v.getUint16(")
							.append("pos")
							.append(");")
							.append("pos+=2;");
						break;
					case INT32:
						sb
							.append("let v")
							.append(i)
							.append("=")
							.append("v.getInt32(")
							.append("pos")
							.append(");")
							.append("pos+=4;");
						break;
					case INT64:
						sb
							.append("let v")
							.append(i)
							.append("=")
							.append("v.getInt32(")
							.append("pos")
							.append(");")
							.append("pos+=8;");
						break;
					case FLOAT32:
						sb
							.append("let v")
							.append(i)
							.append("=")
							.append("v.getFloat32(")
							.append("pos")
							.append(");")
							.append("pos+=4;");
						break;
					case FLOAT64:
						sb
							.append("let v")
							.append(i)
							.append("=")
							.append("v.getFloat64(")
							.append("pos")
							.append(");")
							.append("pos+=8;");
						break;
					case STRING:
						if(!lenDefined){
							sb
								.append("let ");
							lenDefined=true;
						}
						sb
							.append("len=")
							.append("v.getInt32(pos);")
							.append("pos+=4;")
							.append("let v")
							.append(i)
							.append("=")
							.append("decodeUTF8(v.buffer.slice(pos, pos + len));");
						break;
					case BOOLEAN_ARRAY:
					case INT8_ARRAY:
					case INT16_ARRAY:
					case UINT16_ARRAY:
					case INT32_ARRAY:
					case INT64_ARRAY:
					case FLOAT32_ARRAY:
					case FLOAT64_ARRAY:
					case STRING_ARRAY:
						break;
					default:
						throw new RuntimeException();
				}
			}
		}
		sb.append(isb);
		sb.append(");");
		sb.append("}");
		return sb.toString();
	}

}
