package com.jsrmi.ws;

import org.eclipse.jetty.websocket.api.Session;
import org.eclipse.jetty.websocket.api.annotations.*;

@WebSocket(idleTimeout = Integer.MAX_VALUE)
public class JettyWebsocket {
	private final RMISocket ws;

	public JettyWebsocket(RMISocket ws){
		this.ws=ws;
	}
	RMISession session;

	@OnWebSocketMessage
	public void onMessage(byte[] buf, int offset, int length){
		ws.onMessage(buf, offset, length);
	}

	@OnWebSocketConnect
	public void onConnect(Session session){
		this.session=new JettyRMISession(session);
		ws.onConnect(this.session);
	}

	@OnWebSocketError
	public void onError(Throwable error){
		ws.onError(error);
	}

	@OnWebSocketClose
	public void onClose(){
		ws.onClose(this.session);
	}
}
