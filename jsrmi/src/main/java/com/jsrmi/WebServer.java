package com.jsrmi;

import com.jsrmi.js.JSHandlers;
import com.jsrmi.ws.RMIEndpoint;
import com.jsrmi.ws.RMISocket;
import com.jsrmi.ws.RMIWebSocketServlet;
import jakarta.servlet.Servlet;
import java.net.MalformedURLException;
import java.net.URI;
import java.net.URISyntaxException;
import java.net.URL;
import org.eclipse.jetty.server.Server;
import org.eclipse.jetty.server.handler.ContextHandlerCollection;
import org.eclipse.jetty.servlet.DefaultServlet;
import org.eclipse.jetty.servlet.ServletContextHandler;
import org.eclipse.jetty.servlet.ServletHolder;
import org.eclipse.jetty.util.resource.Resource;
import org.eclipse.jetty.websocket.server.config.JettyWebSocketServletContainerInitializer;

import java.util.List;
import java.util.Map;
import java.util.function.BiFunction;

public class WebServer {
  private final Server server;
  private final ServletContextHandler servletContextHandler;

  public WebServer(int port,
                   BiFunction<RMISocket, Map<String, List<String>>, RMIEndpoint> wsCreator)
			throws URISyntaxException, MalformedURLException {
    int serverPort = Integer.getInteger("server.port", port);
    Server server = new Server(serverPort);
    this.server = server;
    ContextHandlerCollection handlers = new ContextHandlerCollection();
    server.setHandler(handlers);
    ServletContextHandler servletContextHandler = new ServletContextHandler();
    this.servletContextHandler = servletContextHandler;
    handlers.addHandler(servletContextHandler);
		servletContextHandler.setContextPath("/");
		ClassLoader cl = WebServer.class.getClassLoader();
		// We look for a file, as ClassLoader.getResource() is not
		// designed to look for directories (we resolve the directory later)
		URL f = cl.getResource("static");
		if (f == null)
		{
			throw new RuntimeException("Unable to find resource directory");
		}

		// Resolve file to directory
		//URI webRootUri = f.toURI().resolve("./").normalize();
		System.err.println("WebRoot is " + f);
		servletContextHandler.setBaseResource(Resource.newResource(f));


    {
      JettyWebSocketServletContainerInitializer.configure(servletContextHandler, null);
      RMIWebSocketServlet websocketServlet = new RMIWebSocketServlet();
      websocketServlet.setWsCreator(wsCreator);
      servletContextHandler.addServlet(new ServletHolder(websocketServlet), "/rmi");
    }


    {
      ServletHolder holder = new ServletHolder(DefaultServlet.class);
      //holder.setInitParameter("resourceBase", "./static/");
      holder.setInitParameter("dirAllowed", "true");
      holder.setInitParameter("pathInfoOnly", "true");
      servletContextHandler.addServlet(holder, "/static/*");
    }

    {
      ServletHolder holder = new ServletHolder(JSHandlers.class);
      servletContextHandler.addServlet(holder, "/rmi_js");
    }
  }

  public void registerServlet(String pathSpec, Servlet servlet) {
    servletContextHandler.addServlet(new ServletHolder(servlet), pathSpec);
  }

  public void start() throws Exception {
    server.start();
    server.join();
  }
}
