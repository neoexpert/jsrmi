package com.jsrmi.js;

import jakarta.servlet.ServletException;
import jakarta.servlet.http.HttpServlet;
import jakarta.servlet.http.HttpServletRequest;
import jakarta.servlet.http.HttpServletResponse;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import static com.jsrmi.ws.Types.*;

public class JSHandlers extends HttpServlet {
    String methods = "";

    @Override
    protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        String signature = req.getParameter("m");
        if (signature != null) {
            String js = genHead(signature);
            resp.getWriter().write(js);
            return;
        }
        resp.getWriter().write(methods);
    }

    private String genHead(String signature) {
        boolean async = signature.startsWith("ASYNC");
        if(async)
            signature=signature.substring(6);
        StringBuilder sb = new StringBuilder();
        char[] arr = signature.toCharArray();
        int pos = readNext(0, arr, sb);
        String rtype = sb.toString();
        ArrayList<String> params = new ArrayList<>();
        while (pos < arr.length) {
            pos = readNext(pos, arr, sb);
            params.add(sb.toString());
        }
        String js = JSBuilder.getHead(async, type(rtype), ptypes(params));
        methods = JSBuilder.genJS();
        return js;
    }

    private static int readNext(int pos, char[] arr, StringBuilder cache) {
        //clear StringBuilder
        cache.setLength(0);
        while (true) {
            char c = arr[pos];
            switch (c) {
                case '_':
                    cache.append('_');
                    ++pos;
                    continue;
                default:
                    cache.append(c);
                    return ++pos;
            }
        }
    }

    private byte[] ptypes(List<String> ptypes) {
        if (ptypes == null)
            return new byte[0];
        byte[] types = new byte[ptypes.size()];
        for (int i = 0; i < types.length; ++i) {
            types[i] = type(ptypes.get(i));
        }
        return types;
    }

    private byte type(String type) {
        switch (type) {
            case "V":
                return VOID;
            case "Z":
                return BOOLEAN;
            case "B":
                return INT8;
            case "S":
                return INT16;
            case "C":
                return UINT16;
            case "I":
                return INT32;
            case "J":
                return INT64;
            case "F":
                return FLOAT32;
            case "D":
                return FLOAT64;
            case "_S":
                return STRING;
            case "_ZA":
                return BOOLEAN_ARRAY;
            case "_SA":
                return INT16_ARRAY;
            case "_CA":
                return UINT16_ARRAY;
            case "_IA":
                return INT32_ARRAY;
            case "_JA":
                return INT64_ARRAY;
            case "_FA":
                return FLOAT32_ARRAY;
            case "_DA":
                return FLOAT64_ARRAY;
            case "__SA":
                return STRING_ARRAY;
            default:
                throw new RuntimeException("unknown type: " + type);

        }
    }
}
