package com.jsrmi.js;

import java.util.Arrays;

class MethodType {
    final byte rtype;
    final byte[] ptypes;
    final int hashCode;
    private final boolean async;

    public MethodType(boolean async, byte rtype, byte[] ptypes) {
        this.async = async;
        this.rtype = rtype;
        this.ptypes = ptypes;
        int hashCode = rtype + Arrays.hashCode(ptypes);
        this.hashCode = hashCode;
    }

    public boolean equals(Object o) {
        MethodType other = (MethodType) o;
        return rtype == other.rtype && Arrays.equals(ptypes, other.ptypes) && async == other.async;

    }

    public int hashCode() {
        return hashCode;
    }
}
