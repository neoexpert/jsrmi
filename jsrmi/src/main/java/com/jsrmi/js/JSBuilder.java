package com.jsrmi.js;

import java.util.*;

import static com.jsrmi.ws.Types.*;

public class JSBuilder {
    static HashMap<MethodType, String> heads = new HashMap<>();

    public synchronized static String getHead(boolean async, byte rtype, byte[] ptypes) {
        MethodType mt = new MethodType(async, rtype, ptypes);
        String js = heads.get(mt);
        if (js == null) {
            js = buildHead(async, rtype, ptypes);
            heads.put(mt, js);
        }
        return js;
    }

    static String s(byte type) {
        switch (type) {
            case VOID:
                return "V";
            case BOOLEAN:
                return "Z";
            case INT8:
                return "B";
            case INT16:
                return "S";
            case UINT16:
                return "C";
            case INT32:
                return "I";
            case INT64:
                return "J";
            case FLOAT32:
                return "F";
            case FLOAT64:
                return "D";
            case STRING:
                return "_S";
            case BOOLEAN_ARRAY:
                return "_ZA";
            case INT8_ARRAY:
                return "_ZA";
            case INT16_ARRAY:
                return "_SA";
            case UINT16_ARRAY:
                return "_CA";
            case INT32_ARRAY:
                return "_IA";
            case INT64_ARRAY:
                return "_JA";
            case FLOAT32_ARRAY:
                return "_FA";
            case FLOAT64_ARRAY:
                return "_DA";
            case STRING_ARRAY:
                return "__SA";
            default:
                throw new RuntimeException();
        }
    }

    private static String buildName(boolean async, byte rtype, byte[] ptypes) {
        StringBuilder sb = new StringBuilder();
        if (async)
            sb.append("ASYNC_");
        sb.append(s(rtype));
        for (byte b : ptypes) {
            sb.append(s(b));
        }

        return sb.toString();
    }

    private static String buildHead(boolean async, byte rtype, byte[] ptypes) {
        StringBuilder sb = new StringBuilder();
        sb.append("async ");
        sb.append("function ");
        String name = buildName(async, rtype, ptypes);
        sb.append(name);
        sb.append("(v, handler)");
        sb.append("{");
        StringBuilder isb = new StringBuilder();
        if (rtype != VOID)
            isb.append("return ");
        isb.append("await handler(");
        int pos;
        if (async)
            pos = 2;
        else pos = 6;
        String prefix = "";
        int i = 0;
        loop:
        for (; i < ptypes.length; ++i) {
            byte type = ptypes[i];
            isb.append(prefix);
            switch (type) {
                case BOOLEAN:
                    isb.append("v.getInt8(")
                            .append(pos)
                            .append(")===1");
                    ++pos;
                    break;
                case INT8:
                    isb.append("v.getInt8(")
                            .append(pos)
                            .append(")");
                    ++pos;
                    break;
                case INT16:
                    isb.append("v.getInt16(")
                            .append(pos)
                            .append(")");
                    pos += 2;
                    break;
                case UINT16:
                    isb.append("v.getUint16(")
                            .append(pos)
                            .append(")");
                    pos += 2;
                    break;
                case INT32:
                    isb.append("v.getInt32(")
                            .append(pos)
                            .append(")");
                    pos += 4;
                    break;
                case INT64:
                    isb.append("v.getInt32(")
                            .append(pos)
                            .append(")");
                    pos += 8;
                    break;
                case FLOAT32:
                    isb.append("v.getFloat32(")
                            .append(pos)
                            .append(")");
                    pos += 4;
                    break;
                case FLOAT64:
                    isb.append("v.getFloat64(")
                            .append(pos)
                            .append(")");
                    pos += 4;
                    break;
                case STRING:
                case BOOLEAN_ARRAY:
                case INT8_ARRAY:
                case INT16_ARRAY:
                case UINT16_ARRAY:
                case INT32_ARRAY:
                case INT64_ARRAY:
                case FLOAT32_ARRAY:
                case FLOAT64_ARRAY:
                case STRING_ARRAY:
                    break loop;
                default:
                    throw new RuntimeException();
            }
            prefix = ",";
        }
        if (i < ptypes.length) {
            boolean lenDefined = false;
            if (async)
                sb.append("let pos=2;");
            else
                sb.append("let pos=6;");
            for (; i < ptypes.length; ++i) {
                byte type = ptypes[i];
                isb.append(prefix);
                prefix = ",";
                isb
                        .append("v")
                        .append(i);
                switch (type) {
                    case BOOLEAN:
                        sb
                                .append("let v")
                                .append(i)
                                .append("=")
                                .append("v.getInt8(")
                                .append("pos++")
                                .append(")===1;");
                        break;
                    case INT8:
                        sb
                                .append("let v")
                                .append(i)
                                .append("=")
                                .append("v.getInt8(")
                                .append("pos++")
                                .append(");");
                        break;
                    case INT16:
                        sb
                                .append("let v")
                                .append(i)
                                .append("=")
                                .append("v.getInt16(")
                                .append("pos")
                                .append(");")
                                .append("pos+=2;");
                        break;
                    case UINT16:
                        sb
                                .append("let v")
                                .append(i)
                                .append("=")
                                .append("v.getUint16(")
                                .append("pos")
                                .append(");")
                                .append("pos+=2;");
                        break;
                    case INT32:
                        sb
                                .append("let v")
                                .append(i)
                                .append("=")
                                .append("v.getInt32(")
                                .append("pos")
                                .append(");")
                                .append("pos+=4;");
                        break;
                    case INT64:
                        sb
                                .append("let v")
                                .append(i)
                                .append("=")
                                .append("v.getInt32(")
                                .append("pos")
                                .append(");")
                                .append("pos+=8;");
                        break;
                    case FLOAT32:
                        sb
                                .append("let v")
                                .append(i)
                                .append("=")
                                .append("v.getFloat32(")
                                .append("pos")
                                .append(");")
                                .append("pos+=4;");
                        break;
                    case FLOAT64:
                        sb
                                .append("let v")
                                .append(i)
                                .append("=")
                                .append("v.getFloat64(")
                                .append("pos")
                                .append(");")
                                .append("pos+=8;");
                        break;
                    case STRING:
                        if (!lenDefined) {
                            sb
                                    .append("let ");
                            lenDefined = true;
                        }
                        sb
                                .append("len=")
                                .append("v.getInt32(pos);")
                                .append("pos+=4;")
                                .append("let v")
                                .append(i)
                                .append("=")
                                .append("decodeUTF8(v.buffer.slice(pos, pos + len));");
                        break;
                    case BOOLEAN_ARRAY:
                    case INT8_ARRAY:
                    case INT16_ARRAY:
                    case UINT16_ARRAY:
                    case INT32_ARRAY:
                    case INT64_ARRAY:
                    case FLOAT32_ARRAY:
                    case FLOAT64_ARRAY:
                    case STRING_ARRAY:
                        break;
                    default:
                        throw new RuntimeException();
                }
            }
        }
        sb.append(isb);
        sb.append(");");
        sb.append("}");
        sb.append(name)
                .append(".rtype=")
                .append(rtype)
                .append(";");
        return sb.toString();
    }


    public synchronized static String genJS() {
        StringBuilder sb = new StringBuilder();
        for (String js : heads.values())
            sb.append(js);
        return sb.toString();
    }
}
