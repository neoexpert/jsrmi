function decodeUTF8(bytes){
	return new TextDecoder("utf-8").decode(bytes);
}
function init_rmi(){
	const VOID = 0;
	const BOOLEAN=1;
	const INT8=2;
	const INT16=3;
	const UINT16=4;
	const INT32=5;
	const INT64=6;
	const FLOAT32=7;
	const FLOAT64=8;
	const STRING = 9;

	const EXCEPTION_RESPONSE = 0;
	const RESPONSE = 1;
	const REGISTER_HANDLER = 2;
	const REGISTER_ASYNC_HANDLER = 3;
	const INVOKE_HANDLER = 4;
	const INVOKE_HANDLER_ASYNC = 5;

	const GENERAL=0;
	const RUNNABLE=1;
	const CONSUMER=2;
	const SUPPLIER=3;
	const FUNCTION=4;
	const AsyncFunction = Object.getPrototypeOf(async function(){}).constructor;

	function loadScript( url, callback ) {
		var script = document.createElement( "script" )
		script.type = "text/javascript";
		if(script.readyState) {  // only required for IE <9
			script.onreadystatechange = function() {
				if ( script.readyState === "loaded" || script.readyState === "complete" ) {
					script.onreadystatechange = null;
					callback();
				}
			};
		} else {  //Others
			script.onload = function() {
				callback();
			};
		}

		script.src = url;
		document.getElementsByTagName( "head" )[0].appendChild( script );
	}

	class WriteAble{
		constructor(){
			this.toWrite=[];
			this.length=0;
		}
		writeByte(value){
			this.length+=1;
			this.toWrite.push(function(pos, dv){
				dv.setInt8(pos, value);
				return 1;
			});
		}

		writeShort(value){
			this.length+=2;
			this.toWrite.push(function(pos, dv){
				dv.setInt16(pos, value);
				return 2;
			});
		}

		writeChar(value){
			this.length+=2;
			this.toWrite.push(function(pos, dv){
				dv.setUint16(pos, value);
				return 2;
			});
		}

		writeInt(value){
			this.length+=4;
			this.toWrite.push(function(pos, dv){
				dv.setInt32(pos, value);
				return 4;
			});
		}

		writeFloat(value){
			this.length+=4;
			this.toWrite.push(function(pos, dv){
				dv.setFloat32(pos, value);
				return 4;
			});
		}

		writeDouble(value){
			this.length+=8;
			this.toWrite.push(function(pos, dv){
				dv.setFloat64(pos, value);
				return 8;
			});
		}

		writeLong(value){
			this.length+=8;
			this.toWrite.push(function(pos, dv){
				dv.setInt32(pos, 0);
				dv.setInt32(pos+4, Number(value));
				return 8;
			});
		}

		writeString(str){
			const strbytes=new TextEncoder("utf-8").encode(str);
			const length=strbytes.length;
			this.writeInt(length);
			this.length+=length;
			this.toWrite.push(function(pos, dv){
				for(let i=0;i<length;++i){
					dv.setInt8(pos++, strbytes[i]);
				}
				return length;
			});
		}
		toArray(){
			const arr=new ArrayBuffer(this.length);
			const dv=new DataView(arr);
			let pos=0;
			const toWrite=this.toWrite;
			const len=toWrite.length;
			for(let i=0;i<len;++i){
				pos += toWrite[i](pos, dv);
			}
			return arr;
		}
	}

	class RMI{
		constructor(params, handlers){
			this.handlers={};
			this.waiters={};
			this.wIDCounter=0;
			var loc = window.location, new_uri;
			if (loc.protocol === "https:") {
				new_uri = "wss:";
			} else {
				new_uri = "ws:";
			}
			new_uri += "//" + loc.host;
			//new_uri += loc.pathname + "/ws";
			new_uri += "/rmi";
			if(params)
				new_uri += "?"+params;
			this.uri=new_uri;
			if(handlers)
				this.handlers=handlers;
			else this.handlers={};
		}

		connect(params){
			var that=this;

			let ws = new WebSocket(this.uri);
			ws.onopen = async function(){
				if(that.onopen)
					that.onopen();
			}
			this.ws=ws;

			ws.onclose=function(){
				console.log("RMI reconnect...");
				if(that.onclose)
					that.onclose();
				ws = null;
				setTimeout(function(){
					that.connect();
				}, 1000);
				//createWebsocket();
			}

			ws.onmessage=async function(evt){
				let msg=evt.data;
				let dv=new DataView(await evt.data.arrayBuffer());
				let cmd=dv.getUint8(0);
				try{
					let arr=null;
					switch(cmd){
						case REGISTER_HANDLER:
							await that.registerServerHandler(dv, false);
							return;
						case REGISTER_ASYNC_HANDLER:
							await that.registerServerHandler(dv, true);
							return;
						case INVOKE_HANDLER:
							let rid=dv.getInt32(1);
							let result = await that.invokeHandler(dv);
							arr=result.rvalue;
							let buffer;
							if(arr==null)
								buffer=new ArrayBuffer(5);
							else 
								buffer=new ArrayBuffer(5 + arr.length);
							dv=new DataView(buffer);
							if(result.exceptionOccured)
								dv.setInt8(0, EXCEPTION_RESPONSE);
							else
								dv.setInt8(0, RESPONSE);
							dv.setInt32(1, rid);

							if(arr!=null)
								for(let i=0;i<arr.length;++i){
									dv.setUint8(i+5, arr[i]);
								}

							ws.send(buffer);
							break;
						case INVOKE_HANDLER_ASYNC:
							await that.invokeHandlerAsync(dv);
							break;
						case RESPONSE:
							that.processResponse(dv);
							return;
						case EXCEPTION_RESPONSE:
							debugger;
							that.processException(dv);
							return;
						default: throw new Error("unknown cmd: "+cmd);
					}
				}
				catch(e){
					console.error(e);
				}

			}

		}

		async findHandler(_async, rtype, ptypes){
			const ts=["V", "Z", "B", "S", "C", "I", "J", "F", "D", "_S"];
			let name="";
			if(_async)
				name+="ASYNC_";
			name+=ts[rtype];
			for(let i=0;i<ptypes.length;++i){
				let tname=ts[ptypes[i]];
				if(!tname)
					throw new Error("unknown type identifier");
				name+=tname;
			}
			const handler=window[name];
			if(handler===undefined){
				const url="/rmi_js?m="+name;
				return new Promise(function(resolve, reject){
					loadScript(url, function(){
						resolve(window[name]);
					});

				});
			}
			return handler;
		}

		async registerServerHandler(dv, _async){
			let id=dv.getUint8(1);
			let rtype=dv.getUint8(2);
			let length=dv.getUint8(3);
			let strbytes = dv.buffer.slice(4, 4 + length);
			let pos = 4 + length;
			let methodName = new TextDecoder("utf-8").decode(strbytes);
			const argsCount = dv.getUint8(pos++);
			let ptypes = new Int8Array(dv.buffer.slice(pos, pos + argsCount));
			let method=this.handlers[methodName];
			if(method===undefined)
				method=window[methodName];
			if(method===undefined){
				throw new Error("undefined function: "+methodName);
			}
			const handler=await this.findHandler(_async, rtype, ptypes);
			if(handler!==null){
				this.handlers[id] = async function(dv){
					return await handler(dv, method);
				}
				this.handlers[id].rtype=handler.rtype;
				return;
			}
			this.handlers[id] = async function(dv){
				let args=new Array(argsCount);
				let pos=2;
				for(let i=0; i < argsCount; ++i){
					let type=ptypes[i];
					switch(type){
						case INT32:
							args[i]=dv.getInt32(pos);
							pos+=4;
							break;
						case STRING:
							let len=dv.getInt32(pos);
							pos+=4;
							let strbytes = dv.buffer.slice(pos, pos + len);
							pos+=len;
							args[i] = new TextDecoder("utf-8").decode(strbytes);
							break;
					}

				}
				await method.apply(null, args);
			}

		}

		async invokeHandler(dv){
			let id=dv.getUint8(5);
			let handler=this.handlers[id];
			let rvalue;
			let result={};
			try{
				rvalue=await handler(dv);
			}
			catch(e){
				result.exceptionOccured=true;
				result.exception=e;
				result.rvalue=new TextEncoder("utf-8").encode(e.message);
				return result;
			}
			switch(handler.rtype){
				case INT32:
					let arrBuf=new ArrayBuffer(4);
					new DataView(arrBuf).setInt32(0, rvalue);
					result.rvalue=new Int8Array(arrBuf);
					break;
				case BOOLEAN:
					let arr=new Int8Array(1);
					arr[0] = rvalue===true?1:0;
					result.rvalue=arr;
					break;
				case STRING:
					if(result==null){
						debugger;
						return new Int8Array(1);
					}
					result.rvalue=new TextEncoder("utf-8").encode(rvalue);
					break;

			}
			return result;
		}

		async invokeHandlerAsync(dv){
			let id=dv.getUint8(1);
			let handler=this.handlers[id];
			await handler(dv);
		}

		registerHandler(id, rtype, argTypes){
			var that=this;
			if(argTypes===undefined){
				const arrBuf=new ArrayBuffer(7);
				let dv=new DataView(arrBuf);
				return async function(){
					const waiterId = ++that.wIDCounter;
					dv.setInt8(0, INVOKE_HANDLER);
					dv.setInt32(1, waiterId);
					dv.setInt8(5, id);
					const rdv=await new Promise(function(resolve, reject){
						that.waiters[waiterId]={resolve:resolve, reject:reject};
						that.ws.send(arrBuf);
					});
					switch(rtype){
						case STRING:
							let strbytes = rdv.buffer.slice(5, rdv.buffer.byteLength);
							let str = new TextDecoder("utf-8").decode(strbytes);
							return str;
						default:throw new Error("unknown type id "+rtype);

					}

				};
			}
			return async function(){
				const waiterId = ++that.wIDCounter;
				let buf=new WriteAble();
				buf.writeByte(INVOKE_HANDLER);
				buf.writeInt(waiterId);
				buf.writeByte(id);
				for(let i=0; i<argTypes.length;++i){
					switch(argTypes[i]){
						case STRING:
							buf.writeString(arguments[i]);
							break;
						case INT32:
							buf.writeInt(arguments[i]);
							break;
						default:
							throw new Error("unsupported argument type id: "+argTypes[i]);

					}
				}
				let arrBuf=buf.toArray();
				const dv=await new Promise(function(resolve, reject){
					that.waiters[waiterId]={resolve:resolve, reject:reject};
					that.ws.send(arrBuf);
				});
				switch(rtype){
					case STRING:
						let strbytes = dv.buffer.slice(5, dv.buffer.byteLength);
						let str = new TextDecoder("utf-8").decode(strbytes);
						return str;
					case VOID:
						return;
					default:throw new Error("unknown type id "+rtype);

				}
			};
		}
		processException(dv){
			let rid=dv.getInt32(1);
			let resolve=this.waiters[rid];
			if(resolve===undefined)
				throw new Error("wrong response id");
			delete this.waiters[rid];
			let length=dv.getInt32(5);
			let strbytes = dv.buffer.slice(9, 9 + length);
			let message = new TextDecoder("utf-8").decode(strbytes);
			resolve.reject(message);
		}

		processResponse(dv){
			let rid=dv.getInt32(1);
			let resolve=this.waiters[rid];
			if(resolve===undefined)
				throw new Error("wrong response id");
			delete this.waiters[rid];
			//let pos=5;
			//let type=dv.getInt8(pos++);
			resolve.resolve(dv);
		}
	}
	RMI.VOID = 0;
	RMI.BOOLEAN=1;
	RMI.INT8=2;
	RMI.INT16=3;
	RMI.UINT16=4;
	RMI.INT32=5;
	RMI.INT64=6;
	RMI.FLOAT32=7;
	RMI.FLOAT64=8;
	RMI.STRING = 9;
	return RMI;
}
