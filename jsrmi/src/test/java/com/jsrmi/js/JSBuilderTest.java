package com.jsrmi.js;


import org.junit.jupiter.api.Test;
import static org.junit.jupiter.api.Assertions.*;
import java.util.concurrent.atomic.AtomicInteger;

import java.util.*;
import static com.jsrmi.ws.Types.*;

public class JSBuilderTest  {

	@Test
	void testRMI() {
		System.out.println(JSBuilder.getHead(true, VOID, new byte[]{INT32}));
		System.out.println(JSBuilder.getHead(true, VOID, new byte[]{STRING}));
		System.out.println(JSBuilder.getHead(false, VOID, new byte[]{INT32}));
		System.out.println(JSBuilder.getHead(false, VOID, new byte[]{STRING}));
		System.out.println("done");
	}

}
