class RMI{
	constructor(){
		this.handlers={};
	}

	connect(){
		var loc = window.location, new_uri;
		if (loc.protocol === "https:") {
			new_uri = "wss:";
		} else {
			new_uri = "ws:";
		}
		new_uri += "//" + loc.host;
		//new_uri += loc.pathname + "/ws";
		new_uri += "/rmi";
		ws = new WebSocket(new_uri);
		ws.onopen = async function(){
			connected=true;
			let notes=await getNotes();
			renderNotes(notes);
		}

		var that=this;
		ws.onclose=function(){
			console.log("RMI reconnect...");
			ws = null;
			connected=false;
			cleanUp();
			setTimeout(function(){
				that.connect();
			}, 1000);
			//createWebsocket();
		}

		ws.onmessage=async function(evt){
			let msg=evt.data;
			let dv=new DataView(await evt.data.arrayBuffer());
			let rid=dv.getInt32(0);
			try{
				let cmd=dv.getUint8(4);
				let arr=null;
				switch(cmd){
					case 1:
						that.registerHandler(dv);
						break;
					case 2:
						arr = await that.invokeHandler(dv);
						break;
					default: throw new Error("unknown cmd: "+cmd);
				}
				let buffer;
				if(arr==null)
					buffer=new ArrayBuffer(4);
				else 
					buffer=new ArrayBuffer(4 + arr.length);
				dv=new DataView(buffer);
				dv.setInt32(0, rid);

				if(arr!=null)
					for(let i=0;i<arr.length;++i){
						dv.setUint8(i+4, arr[i]);
					}

				ws.send(buffer);
			}
			catch(e){
				debugger;
				console.error(e);
			}

		}

	}
	registerHandler(dv){
		let id=dv.getUint8(5);
		let rtype=dv.getUint8(6);
		let length=dv.getUint8(7);
		let strbytes = dv.buffer.slice(8, 8 + length);
		let methodName = new TextDecoder("utf-8").decode(strbytes);
		this.handlers[id] = {handler: window[methodName], rtype: rtype};
	}
	invokeHandler(dv){
		const _I=73;
		const _Z=90;
		const _STRING=83;
		let id=dv.getUint8(5);
		let argsCount=dv.getUint8(6);
		let args=new Array(argsCount);
		let pos=7;
		for(let i=0; i < argsCount; ++i){
			let type=dv.getUint8(pos++);
			switch(type){
				case _I:
					args[i]=dv.getInt32(pos);
					pos+=4;
					break;
				case _STRING:
					let len=dv.getInt32(pos);
					pos+=4;
					let strbytes = dv.buffer.slice(pos, pos + len);
					pos+=len;
					args[i] = new TextDecoder("utf-8").decode(strbytes);
					break;
			}

		}
		let handler=this.handlers[id];
		let result=handler.handler.apply(null, args);
		switch(handler.rtype){
			case _I:
				let arrBuf=new ArrayBuffer(4);
				new DataView(arrBuf).setInt32(0, result);
				return new Int8Array(arrBuf);
			case _Z:
				let arr=new Int8Array(1);
				arr[0] = result===true?1:0;
				return arr;
			case _STRING:
				return new TextEncoder("utf-8").encode(result);

		}
		return null;
	}
}
