function setParameter(key, value){
	key = encodeURIComponent(key);
	value = encodeURIComponent(value);

	// kvp looks like ['key1=value1', 'key2=value2', ...]
	var kvp = document.location.search.substr(1).split('&');
	let i=0;

	for(; i<kvp.length; i++){
		if (kvp[i].startsWith(key + '=')) {
			let pair = kvp[i].split('=');
			pair[1] = value;
			kvp[i] = pair.join('=');
			break;
		}
	}

	if(i >= kvp.length){
		kvp[kvp.length] = [key,value].join('=');
	}

	// can return this or...
	let params = kvp.join('&');

	// reload page with new params
	//document.location.search = params;
	history.replaceState(null, '', "?"+params);
}

function removeParameter(key){
	key = encodeURIComponent(key);

	// kvp looks like ['key1=value1', 'key2=value2', ...]
	var kvp = document.location.search.substr(1).split('&');
	let i=0;

	let index=-1;
	for(; i<kvp.length; i++){
		if (kvp[i].startsWith(key + '=')) {
			index=i;
			break;
		}
	}
	if(index>-1)
		kvp.splice(index, 1);

	// can return this or...
	let params = kvp.join('&');

	// reload page with new params
	//document.location.search = params;
	history.replaceState(null, '', "?"+params);
}
function getParameter(key){
	key = encodeURIComponent(key);

	// kvp looks like ['key1=value1', 'key2=value2', ...]
	var kvp = document.location.search.substr(1).split('&');
	let i=0;

	for(; i<kvp.length; i++){
		if (kvp[i].startsWith(key + '=')) {
			let pair = kvp[i].split('=');
			if(pair[0]==key)
				return decodeURIComponent(pair[1]);
			//pair[1] = value;
			kvp[i] = pair.join('=');
			break;
		}
	}


	// can return this or...
	let params = kvp.join('&');


	// reload page with new params
	//document.location.search = params;
	return null;
}
function randColor2(){
  return "hsl(" + 360 * Math.random() + ',' +
			 (25 + 70 * Math.random()) + '%,' +
			 (75 + 20 * Math.random()) + '%)'
}

function isTouchDevice() {
  return (('ontouchstart' in window) ||
	 (navigator.maxTouchPoints > 0) ||
	 (navigator.msMaxTouchPoints > 0));
}
var createNoteOnServer;
async function createNewNote(){
	await createNoteOnServer(randColor2());
}

/*
function updateNotePos(obj){
	let element = document.getElementById(obj.id);
	element=element.instance;
	element.translation.setTranslate(obj.obj.x, obj.obj.y);
}*/

function deleteNote(id){
	let element = document.getElementById(id);
	element.parentNode.removeChild(element);
}
function bringToFront(id){
	let element = document.getElementById(id);
	element.parentNode.appendChild(element);
}

function throwException(id){
	throw new Error("some exception");
}

async function sleep(duration){
	return new Promise(function(resolve, reject){
		setTimeout(resolve, duration);
	});
}

function init_notes(){

	function cleanUp(){
		createButton.setAttribute("hidden", true);
		var elements = document.getElementsByClassName('draggable');
		while(elements[0]) {
			elements[0].parentNode.removeChild(elements[0]);
		}
	}

	const body = document.body;
	const root = document.getElementById("root");

	var downX, downY;
	var mapMouseDown=false;
	const map = document.getElementById("map");
	root.addEventListener('mousedown', function(e){
		if(e.button!=0)
			return;
		mapMouseDown=true;
		downX = e.clientX;
		downY = e.clientY;
	});
	if(isTouchDevice())
		root.addEventListener('touchstart', function(e){
			mapMouseDown=true;
			let touch = e.changedTouches[0];
			downX = touch.clientX;
			downY = touch.clientY;
		});
	const mapTranslation = root.createSVGTransform();
	map.translation=mapTranslation;
	map.transform.baseVal.appendItem(mapTranslation);


	let mapX=getParameter("x");
	let mapY=getParameter("y");
	if(mapX!=null&&mapY!=null){
		mapX=parseInt(mapX);
		mapY=parseInt(mapY);
		map.dx=mapX;
		map.dy=mapY;
		mapTranslation.setTranslate(mapX, mapY);
	}
	else{
		map.dx=0;
		map.dy=0;
	}

	const createButton = document.getElementById("create");
	var _draggable=null;

	let connected=false;
	const CREATE = 1;
	const UPDATE_POSITION = 3;
	const BRING_TO_FRONT = 7;
	const UPDATE = 4;
	const GET = 5;
	const DELETE = 6;

	let params="room="+room;
	let id=localStorage.id;
	if(id){
		userID=id;
		params+="&id="+id;
	}

	const RMI=init_rmi();

	const handlers={};
	var userID;
	handlers.setUserID = function(id){
		localStorage.id=id;
		userID=id;
	}

	handlers.updateNotePosition = function(id, x, y){
		let element = document.getElementById(id);
		element=element.instance;
		element.translation.setTranslate(x, y);
	}

	handlers.updateNote = function(json){
		const obj=JSON.parse(json);
		let element = document.getElementById(obj.id);
		element=element.instance;
		element.translation.setTranslate(obj.x, obj.y);
		element.dx=obj.x;
		element.dy=obj.y;
		element.textArea.value=obj.text;
	}
	const rmi=new RMI(params, handlers);
	var getNotes;
	var updateNotePositionOnServer;
	var bringNoteToFrontOnServer;
	var updateNoteOnServer;
	var deleteNoteOnServer;
	rmi.onopen=async function(){
		getNotes=rmi.registerHandler(GET, RMI.STRING);
		createNoteOnServer=rmi.registerHandler(CREATE, RMI.VOID, [RMI.STRING]);
		updateNotePositionOnServer=rmi.registerHandler(UPDATE_POSITION, RMI.VOID, [RMI.INT32, RMI.INT32, RMI.INT32]);
		bringNoteToFrontOnServer=rmi.registerHandler(BRING_TO_FRONT, RMI.VOID, [RMI.INT32]);
		updateNoteOnServer=rmi.registerHandler(UPDATE, RMI.VOID, [RMI.INT32, RMI.STRING]);
		deleteNoteOnServer=rmi.registerHandler(DELETE, RMI.VOID, [RMI.INT32]);
		let json=await getNotes();
		let notes=JSON.parse(json);
		renderNotes(notes);
		createButton.removeAttribute("hidden");
		connected=true;
	}
	rmi.onclose=function(){
		connected=false;
		cleanUp();
	}
	rmi.connect();



	const viewers=document.getElementById("viewers");
	handlers.setViewerCount=function(count){
		viewers.innerText="viewers: "+count;
	}

	function renderNotes(notes){
		let keys=Object.keys(notes);
		let size=keys.length;
		for(let i=0;i<size;++i){
			let note=notes[keys[i]];
			createNote(note);
		}
	}

	handlers.createNoteFromJSON=function(json){
		createNote(JSON.parse(json));
	}

	class Draggable {
		constructor(id, x, y, lock){
			const draggable = document.createElementNS("http://www.w3.org/2000/svg", "g");
			const draggableArea = document.createElementNS("http://www.w3.org/2000/svg", "g");
			this.draggableArea=draggableArea;
			draggable.appendChild(draggableArea);
			this.draggableArea=draggableArea;
			draggable.classList.add("draggable");
			this.node=draggable;
			draggable.instance=this;
			const translation=root.createSVGTransform();
			this.translation=translation;
			this.translation=translation;
			draggable.transform.baseVal.appendItem(translation);
			draggable.setAttribute("id", id);
			this.id=id;
			this.dx=x;
			this.dy=y;
			translation.setTranslate(x, y);
			this.finger=-1;
			if(lock!==true)
				this.makeDraggable();

		}

		makeDraggable(){
			const draggable=this.node;
			const draggableArea=this.draggableArea;
			var that=this;
			if(isTouchDevice()){
				function touchmove(e){
					for(let touch of e.changedTouches){
						if(touch.identifier===that.finger){
							let tx = touch.clientX;
							let ty = touch.clientY;
							let dx = tx - that.downX;
							let dy = ty - that.downY;
							let newX=(that.dx + dx);
							let newY=(that.dy + dy);
							//draggable.style.left = newX+"px";
							//draggable.style.top = newY+"px";
							that.translation.setTranslate(newX, newY);
							updateNotePositionOnServer(that.id, newX, newY);
						}
					}
				}
				function touchend(e){
					for(let touch of e.changedTouches){
						if(touch.identifier===that.finger){
							window.removeEventListener('touchmove', touchmove);
							window.removeEventListener('touchend', touchend);
							that.finger=-1;
							let dx = touch.clientX - that.downX;
							let dy = touch.clientY - that.downY;
							that.dx += dx;
							that.dy += dy;
							let newX=(that.dx)+"px";
							let newY=(that.dy)+"px";
							updateNoteOnServer(that.id, JSON.stringify({x:that.dx, y:that.dy}));

						}
					}
				}
				draggableArea.addEventListener('touchstart', function(e){
					let touch = e.changedTouches[0];
					that.finger=touch.identifier;
					that.downX = touch.clientX;
					that.downY = touch.clientY;
					e.stopPropagation();
					window.addEventListener('touchmove', touchmove);
					window.addEventListener('touchend', touchend);
					bringNoteToFrontOnServer(that.id);
				});
			}
			draggableArea.addEventListener('mousedown', function(e){
				if(e.button!=0)
					return;
				_draggable=that;
				downX = e.clientX;
				downY = e.clientY;
				e.stopPropagation();
				bringNoteToFrontOnServer(that.id);
			});
		}

		appendChild(child){
			this.node.appendChild(child);
		}
		appendToDraggableArea(child){
			this.draggableArea.appendChild(child);
		}
	}

	class Note extends Draggable{
		constructor(note){
			super(note.id, note.x, note.y, true);
			const draggableArea = document.createElementNS("http://www.w3.org/2000/svg", "rect");
			draggableArea.setAttribute("width", "236px");
			draggableArea.setAttribute("height", "16px");
			draggableArea.setAttribute("y", "-20");
			draggableArea.setAttribute("fill", note.color);
			this.appendToDraggableArea(draggableArea);

			const rectBG = document.createElementNS("http://www.w3.org/2000/svg", "rect");
			rectBG.setAttribute("width", "256px");
			rectBG.setAttribute("height", "256px");
			rectBG.setAttribute("fill", note.color);
			this.appendChild(rectBG);

			const foreignObject = document.createElementNS("http://www.w3.org/2000/svg", "foreignObject");
			foreignObject.setAttribute("width", "256px");
			foreignObject.setAttribute("height", "256px");
			this.appendChild(foreignObject);

			const textArea = document.createElement("textarea");
			textArea.setAttribute("maxlength", "430");
			this.textArea=textArea;
			textArea.value=note.text;
			textArea.classList.add("transparent");
			textArea.classList.add("wide");
			textArea.addEventListener("input",function(){
				updateNoteOnServer(note.id, JSON.stringify({"text":textArea.value}));
			});
			foreignObject.appendChild(textArea);
			if(note.owner==userID){
				this.makeDraggable();
				const deleteButton = document.createElementNS("http://www.w3.org/2000/svg", "rect");
				deleteButton.addEventListener("click", function(){
					deleteNoteOnServer(note.id);
				});
				deleteButton.setAttribute("x", "240px");
				deleteButton.setAttribute("y", "-20px");
				deleteButton.setAttribute("width", "16px");
				deleteButton.setAttribute("height", "16px");
				deleteButton.setAttribute("fill", note.color);
				this.appendChild(deleteButton);
			}
			else{
				textArea.readOnly=true;
			}

		}
	}

	function createNote(note){
		const _note=new Note(note);
		map.appendChild(_note.node);
	}


	window.addEventListener('mousemove', e => {
		if(_draggable!=null){
			let dx = e.clientX - downX;
			let dy = e.clientY - downY;
			let newX=(_draggable.dx + dx);
			let newY=(_draggable.dy + dy);
			//_draggable.style.left = newX + "px";
			//_draggable.style.top = newY + "px";

			_draggable.translation.setTranslate(newX, newY);
			updateNotePositionOnServer(_draggable.id, newX, newY);
			e.stopPropagation();
			e.preventDefault();
		}
		else if(mapMouseDown){
			let dx = e.clientX - downX;
			let dy = e.clientY - downY;
			let newX=(map.dx + dx);
			let newY=(map.dy + dy);
			mapTranslation.setTranslate(newX, newY);
			e.stopPropagation();
			e.preventDefault();
		}
	});

	window.addEventListener('mouseup', e => {
		if(_draggable!=null){
			let dx = e.clientX - downX;
			let dy = e.clientY - downY;
			_draggable.dx += dx;
			_draggable.dy += dy;
			updateNoteOnServer(_draggable.id, JSON.stringify({x:_draggable.dx, y:_draggable.dy}));
		}
		else if(mapMouseDown){
			mapMouseDown=false;

			let dx = e.clientX - downX;
			let dy = e.clientY - downY;
			map.dx += dx;
			map.dy += dy;
			setParameter("x", map.dx);
			setParameter("y", map.dy);
			//e.stopPropagation();
		}
		_draggable=null;
	});
	if(isTouchDevice()){
		window.addEventListener('touchmove', e => {
			if(mapMouseDown){
				let touch = e.changedTouches[0];
				let dx = touch.clientX - downX;
				let dy = touch.clientY - downY;
				let newX=(map.dx + dx);
				let newY=(map.dy + dy);
				mapTranslation.setTranslate(newX, newY);
			}
		});
		window.addEventListener('touchend', e => {
			if(mapMouseDown){
				mapMouseDown=false;
				let touch = e.changedTouches[0];
				let dx = touch.clientX - downX;
				let dy = touch.clientY - downY;
				map.dx += dx;
				map.dy += dy;
				setParameter("x", map.dx);
				setParameter("y", map.dy);
				//e.stopPropagation();
			}
			_draggable=null;
		});
	}
}
init_notes();

