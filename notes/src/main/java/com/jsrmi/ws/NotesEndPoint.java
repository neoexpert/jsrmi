package com.jsrmi.ws;

import org.json.JSONObject;

import java.util.Map;
import java.util.concurrent.ConcurrentHashMap;

public class NotesEndPoint {
	private final Window window;
	private final ConcurrentHashMap<RMISocket, Window> sessions;
	private final ConcurrentHashMap<Integer, Note> notes;
	private final NotesRoom workspace;
	private final User user;

	public NotesEndPoint(User u, NotesRoom workspace, Window window, ConcurrentHashMap<RMISocket, Window> sessions, ConcurrentHashMap<Integer, Note> notes) {
		this.user=u;
		this.workspace=workspace;
		this.window=window;
		this.sessions=sessions;
		this.notes=notes;
	}

	@Remote(id = 3)
	public void updateNotePosition(int id, int x, int y){
		for(Map.Entry<RMISocket, Window> entry : sessions.entrySet()) {
			Window w=entry.getValue();
			if(w==window)
				continue;
			try {
				w.updateNotePosition(id, x, y);
			}
			catch (RuntimeException e){
				entry.getKey().close();
			}
		}
	}

	@Remote(id = 7)
	public void bringToFront(int id){
		for(Map.Entry<RMISocket, Window> entry : sessions.entrySet()) {
			Window w=entry.getValue();
			try {
				w.bringToFront(id);
			}
			catch (RuntimeException e){
				entry.getKey().close();
			}
		}
	}

	@Remote(id = 6)
	public void deleteNote(int id){
		Note note = notes.remove(id);
		if(note==null)
			throw new RuntimeException("no such note: "+id);
		for(Map.Entry<RMISocket, Window> entry : sessions.entrySet()) {
			Window w=entry.getValue();
			try {
				w.deleteNote(id);
			}
			catch (RuntimeException e){
				e.printStackTrace();
				entry.getKey().close();
			}
		}
		workspace.store();
	}

	public void createNote(String color){
		Note note=workspace.createNote(user, color);
		String toSend=note.toJSON().toString();
		for(Map.Entry<RMISocket, Window> w : sessions.entrySet()) {
			try {
				w.getValue().createNoteFromJSON(toSend);
			}
			catch (RuntimeException e){
				w.getKey().close();
			}
		}
		workspace.store();
	}

	@Remote(id = 4)
	public void updateNote(int id, String json){
		JSONObject jo=new JSONObject(json);
		Note note = notes.get(id);
		if(note==null)
			throw new RuntimeException("no such note: "+id);
		note.update(jo);
		final String toSend=note.toJSON().toString();
		for(Map.Entry<RMISocket, Window> entry : sessions.entrySet()) {
			Window w=entry.getValue();
			if(w==window)
				continue;
			try {
				w.updateNote(toSend);
			}
			catch (RuntimeException e){
				e.printStackTrace();
				entry.getKey().close();
			}
		}
		workspace.store();
	}

}
