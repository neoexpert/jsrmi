package com.jsrmi.ws;

public interface Window {
	@Remote(id=1)
	void alert(String message);
	@Remote(id=2)
	boolean confirm(String message);
	@Remote(id=3)
	boolean isTouchDevice();
	@Remote(id=4)
	String prompt(String message);
	@Remote(id=5)
	void sleep(int duration);
	@Remote(id=6)
	void throwException();
	@Async
	@Remote(id=7)
	void createNoteFromJSON(String string);

	@Async
	@Remote(id=8)
	void updateNotePosition(int id, int x, int y);
	@Remote(id=13)
	@Async
	void bringToFront(int id);

	@Async
	@Remote(id=9)
	void updateNote(String json);

	@Async
	@Remote(id=10)
	void setViewerCount(int count);

	@Async
	@Remote(id=11)
	void deleteNote(int id);

	@Async
	@Remote(id=12)
	void setUserID(String id);
}
