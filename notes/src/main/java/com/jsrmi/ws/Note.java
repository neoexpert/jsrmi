package com.jsrmi.ws;

import org.json.JSONObject;

public class Note {
    private final int id;
    private final JSONObject obj;
    private final String owner;
    private int zindex;

    public Note(String userID, int id, String color) {
        this.owner = userID;
        this.id = id;
        this.obj = new JSONObject();
        obj.put("owner", userID);
        obj.put("color", color);
        obj.put("id", id);
        obj.put("x", 0);
        obj.put("y", 0);
        obj.put("text", "");
        obj.put("zindex", 0);
    }

    public Note(JSONObject serialized) {
        this.obj = serialized;
        this.id = serialized.getInt("id");
        if(serialized.has("zindex"))
            this.zindex = serialized.getInt("zindex");
        if (serialized.has("owner"))
            this.owner = serialized.getString("owner");
        else owner = null;
    }

    public void update(JSONObject obj) {
        for (String key : obj.keySet()) {
            this.obj.put(key, obj.get(key));
        }
        this.obj.put("id", id);
        this.obj.put("owner", owner);
    }

    public JSONObject toJSON() {
        return obj;
    }

    public int getZIndex() {
        return zindex;
    }
}
