package com.jsrmi.ws;

import com.jsrmi.WebServer;

import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.function.BiFunction;

public class Server {
	private final int port;
	HashMap<String, NotesRoom> rooms = new HashMap<>();

	public Server(int port) {
		this.port=port;
	}


	public static void main(String[] args) throws Exception {
		int port=8081;
		for(int i=0;i<args.length;++i){
			String p=args[i];
			switch (p){
				case "-p":
					port=Integer.parseInt(args[++i]);
					break;
			}
		}
		Server server = new Server(port);
		server.start();
	}

	private void start() throws Exception {
		WebServer webServer = new WebServer(port, new BiFunction<RMISocket, Map<String, List<String>>, RMIEndpoint>() {
			@Override
			public RMIEndpoint apply(RMISocket rmiWebSocket, Map<String, List<String>> params) {
				String roomName = getParameter(params, "room");
				if(roomName==null)
					roomName="root";
				if(roomName.startsWith("/"))
					roomName=roomName.substring(1);
				NotesRoom room = rooms.get(roomName);
				if(room==null){
					room=new NotesRoom(roomName);
					rooms.put(roomName, room);
				}
				return room;
			}
		});
		webServer.registerServlet("/*", new HelloServlet());
		webServer.start();
	}

	public static String getParameter(Map<String, List<String>> params, String name){
		List<String> values = params.get(name);
		if(values==null)
			return null;
		if(values.isEmpty())
			return null;
		return values.get(0);
	}
}