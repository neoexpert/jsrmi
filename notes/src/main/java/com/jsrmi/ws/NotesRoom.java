package com.jsrmi.ws;


import static com.jsrmi.ws.Server.getParameter;
import static com.jsrmi.ws.Types.STRING;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.UUID;
import java.util.concurrent.ConcurrentHashMap;
import org.json.JSONObject;

public class NotesRoom implements RMIEndpoint {
  private static final File root;
  int maxZIndex;

  static {
    root = new File("rooms");
    if (!root.exists()) {
      root.mkdir();
    }
  }

  private final File file;
  ConcurrentHashMap<RMISocket, Window> sessions = new ConcurrentHashMap<>();
  ConcurrentHashMap<Integer, Note> notes = new ConcurrentHashMap<>();
  private int idCounter;

  public NotesRoom(String roomName) {
    if (roomName.contains("..")) {
      throw new RuntimeException("room name may not contain \"..\"");
    }
    file = new File(root, roomName + ".json");
    load();
  }

  private synchronized void load() {
    if (!file.exists()) {
      return;
    }
    try (FileInputStream fis = new FileInputStream(file)) {
      BufferedReader br = new BufferedReader(new InputStreamReader(fis));
      String str = br.readLine();
      if (str.startsWith("[")) {
        throw new RuntimeException("not an JSONObject");
      }
      if (str.startsWith("{")) {
        JSONObject notes = new JSONObject(str);
        readNotes(notes);
        return;
      }
    } catch (IOException e) {
      throw new RuntimeException(e);
    }
  }

  private void readNotes(JSONObject notes) {
    int maxID = Integer.MIN_VALUE;
    Set<String> keys = notes.keySet();
    for (String id : keys) {
      int _id = Integer.parseInt(id);
      if (_id > maxID) {
        maxID = _id;
      }
      Note note = new Note(notes.getJSONObject(id));
      if (maxZIndex < note.getZIndex()) {
        maxZIndex = note.getZIndex();
      }
      this.notes.put(_id, note);
    }
    this.idCounter = maxID + 1;
  }


  public synchronized void store() {
    try (FileOutputStream fos = new FileOutputStream(file)) {
      fos.write(getNotes().getBytes());
    } catch (IOException e) {
      throw new RuntimeException(e);
    }
  }

  public String getNotes() {
    JSONObject notes = new JSONObject();
    for (Map.Entry<Integer, Note> n : this.notes.entrySet()) {
      notes.put(n.getKey().toString(), n.getValue().toJSON());
    }
    return notes.toString();
  }


  @Override
  public void onConnect(RMISocket ws, Map<String, List<String>> params) {
    ws.registerServerSupplier((byte) 5, this::getNotes, STRING);
    Window window = ws.registerClientHandlers(Window.class);
    String userID = getParameter(params, "id");
    if (userID == null) {
      userID = UUID.randomUUID().toString();
      window.setUserID(userID);
    }
    User u = new User(userID);

    NotesEndPoint nep = new NotesEndPoint(u, this, window, sessions, notes);
    ws.registerServerConsumer((byte) 1, nep::createNote, STRING);
    ws.registerServerHandlers(nep);
    sessions.put(ws, window);
    int count = sessions.size();
    for (Map.Entry<RMISocket, Window> w : sessions.entrySet()) {
      try {
        w.getValue().setViewerCount(count);
      } catch (RuntimeException e) {
        w.getKey().close();
      }
    }
    Thread demoThread = new Thread(new Runnable() {
      @Override
      public void run() {
        while (true) {
          runDemo(window);
        }

      }
    });
    //demoThread.start();
  }

  private void runDemo(Window window) {
    window.alert("test");

    System.out.println(window.prompt("test"));

    window.sleep(1000);

    window.sleep(1000);

    window.sleep(10000);

    try {
      window.throwException();
    } catch (Exception e) {
      System.out.println(e.getMessage());
      e.printStackTrace();
    }
  }


  @Override
  public void onDisconnect(RMISocket ws) {
    Window result = sessions.remove(ws);
    if (result == null) {
      throw new RuntimeException("disconnection event was fired on an invalid endpoint");
    }
    int count = sessions.size();
    for (Map.Entry<RMISocket, Window> w : sessions.entrySet()) {
      try {
        w.getValue().setViewerCount(count);
      } catch (RuntimeException e) {
        w.getKey().close();
      }
    }
  }

  public Note createNote(User user, String color) {
    int id = idCounter++;
    Note note = new Note(user.getID(), id, color);
    notes.put(id, note);
    return note;
  }

  public int getMaxZIndex() {
    return maxZIndex;
  }
}
