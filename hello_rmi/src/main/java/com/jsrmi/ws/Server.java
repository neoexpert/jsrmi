package com.jsrmi.ws;

import com.jsrmi.WebServer;

import java.util.List;
import java.util.Map;
import java.util.function.BiFunction;

public class Server implements BiFunction<RMISocket, Map<String, List<String>>, RMIEndpoint>, RMIEndpoint {


	public static void main(String[] args) throws Exception {
		Server server = new Server();
		server.start();
	}

	private void start() throws Exception {
		WebServer webServer = new WebServer(8081,this);
		webServer.start();
	}

	@Override
	public RMIEndpoint apply(RMISocket rmiWebSocket, Map<String, List<String>> stringListMap) {
		return this;
	}

	interface Window{
		String getHello(String message);
	}

	Window window;
	@Override
	public void onConnect(RMISocket ws, Map<String, List<String>> params) {
		window=ws.registerClientHandlers(Window.class);
		ws.registerServerHandlers(this);
		new Thread(this::handleClient).start();
	}

	private void handleClient() {
		System.out.println(window.getHello(" world"));
	}

	public void println(String message){
		System.out.println("invoked from browser: "+message);
	}

	@Override
	public void onDisconnect(RMISocket rmiWebSocket) {

	}
}