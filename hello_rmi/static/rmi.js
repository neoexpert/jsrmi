const EXCEPTION_RESPONSE=4;
const RESPONSE=0;
const REGISTER_HANDLER=1;
const INVOKE_HANDLER=2;
const INVOKE_HANDLER_ASYNC=3;
const _I=73;
const _Z=90;
const _STRING=83;
const GENERAL=0;
const RUNNABLE=1;
const CONSUMER=2;
const SUPPLIER=3;
const FUNCTION=4;
class WriteAble{
	constructor(){
		this.toWrite=[];
		this.length=0;
	}
	writeByte(value){
		this.length+=1;
		this.toWrite.push(function(pos, dv){
			dv.setInt8(pos, value);
			return 1;
		});
	}

	writeShort(value){
		this.length+=2;
		this.toWrite.push(function(pos, dv){
			dv.setInt16(pos, value);
			return 2;
		});
	}

	writeChar(value){
		this.length+=2;
		this.toWrite.push(function(pos, dv){
			dv.setUint16(pos, value);
			return 2;
		});
	}

	writeInt(value){
		this.length+=4;
		this.toWrite.push(function(pos, dv){
			dv.setInt32(pos, value);
			return 4;
		});
	}

	writeFloat(value){
		this.length+=4;
		this.toWrite.push(function(pos, dv){
			dv.setFloat32(pos, value);
			return 4;
		});
	}

	writeDouble(value){
		this.length+=8;
		this.toWrite.push(function(pos, dv){
			dv.setFloat64(pos, value);
			return 8;
		});
	}

	writeLong(value){
		this.length+=8;
		this.toWrite.push(function(pos, dv){
			dv.setInt32(pos, 0);
			dv.setInt32(pos+4, Number(value));
			return 8;
		});
	}

	writeString(str){
		const strbytes=new TextEncoder("utf-8").encode(str);
		const length=strbytes.length;
		this.writeInt(length);
		this.length+=length;
		this.toWrite.push(function(pos, dv){
			for(let i=0;i<length;++i){
				dv.setInt8(pos++, strbytes[i]);
			}
			return length;
		});
	}
	toArray(){
		const arr=new ArrayBuffer(this.length);
		const dv=new DataView(arr);
		let pos=0;
		const toWrite=this.toWrite;
		const len=toWrite.length;
		for(let i=0;i<len;++i){
			pos += toWrite[i](pos, dv);
		}
		return arr;
	}
}

class RMI{
	constructor(params){
		this.handlers={};
		this.waiters={};
		this.wIDCounter=0;
		var loc = window.location, new_uri;
		if (loc.protocol === "https:") {
			new_uri = "wss:";
		} else {
			new_uri = "ws:";
		}
		new_uri += "//" + loc.host;
		//new_uri += loc.pathname + "/ws";
		new_uri += "/rmi";
		if(params)
			new_uri += "?"+params;
		this.uri=new_uri;
	}

	connect(params){
		var that=this;

		let ws = new WebSocket(this.uri);
		ws.onopen = async function(){
			if(that.onopen)
				that.onopen();
		}
		this.ws=ws;

		ws.onclose=function(){
			console.log("RMI reconnect...");
			if(that.onclose)
				that.onclose();
			ws = null;
			setTimeout(function(){
				that.connect();
			}, 1000);
			//createWebsocket();
		}

		ws.onmessage=async function(evt){
			let msg=evt.data;
			let dv=new DataView(await evt.data.arrayBuffer());
			let cmd=dv.getUint8(0);
			try{
				let arr=null;
				switch(cmd){
					case REGISTER_HANDLER:
						that.registerServerHandler(dv);
						return;
					case INVOKE_HANDLER:
						let rid=dv.getInt32(1);
						let result = await that.invokeHandler(dv);
						arr=result.rvalue;
						let buffer;
						if(arr==null)
							buffer=new ArrayBuffer(5);
						else 
							buffer=new ArrayBuffer(5 + arr.length);
						dv=new DataView(buffer);
						if(result.exceptionOccured)
							dv.setInt8(0, EXCEPTION_RESPONSE);
						else
							dv.setInt8(0, RESPONSE);
						dv.setInt32(1, rid);

						if(arr!=null)
							for(let i=0;i<arr.length;++i){
								dv.setUint8(i+5, arr[i]);
							}

						ws.send(buffer);
						break;
					case INVOKE_HANDLER_ASYNC:
						await that.invokeHandlerAsync(dv);
						break;
					case RESPONSE:
						that.processResponse(dv);
						return;
					default: throw new Error("unknown cmd: "+cmd);
				}
			}
			catch(e){
				debugger;
				console.error(e);
			}

		}

	}

	registerServerHandler(dv){
		let id=dv.getUint8(1);
		let rtype=dv.getUint8(2);
		let length=dv.getUint8(3);
		let strbytes = dv.buffer.slice(4, 4 + length);
		let methodName = new TextDecoder("utf-8").decode(strbytes);
		this.handlers[id] = {handler: window[methodName], rtype: rtype};
	}

	async invokeHandler(dv){
		let id=dv.getUint8(6);
		let argsCount=dv.getUint8(7);
		let args=new Array(argsCount);
		let pos=8;
		for(let i=0; i < argsCount; ++i){
			let type=dv.getUint8(pos++);
			switch(type){
				case _I:
					args[i]=dv.getInt32(pos);
					pos+=4;
					break;
				case _STRING:
					let len=dv.getInt32(pos);
					pos+=4;
					let strbytes = dv.buffer.slice(pos, pos + len);
					pos+=len;
					args[i] = new TextDecoder("utf-8").decode(strbytes);
					break;
			}

		}
		let handler=this.handlers[id];
		let rvalue;
		let result={};
		try{
			rvalue=await handler.handler.apply(null, args);
		}
		catch(e){
			result.exceptionOccured=true;
			result.exception=e;
			result.rvalue=new TextEncoder("utf-8").encode(e.message);
			return result;
		}
		switch(handler.rtype){
			case _I:
				let arrBuf=new ArrayBuffer(4);
				new DataView(arrBuf).setInt32(0, rvalue);
				result.rvalue=new Int8Array(arrBuf);
				break;
			case _Z:
				let arr=new Int8Array(1);
				arr[0] = rvalue===true?1:0;
				result.rvalue=arr;
				break;
			case _STRING:
				if(result==null){
					debugger;
					return new Int8Array(1);
				}
				result.rvalue=new TextEncoder("utf-8").encode(rvalue);
				break;

		}
		return result;
	}

	async invokeHandlerAsync(dv){
		let id=dv.getUint8(2);
		let argsCount=dv.getUint8(3);
		let args=new Array(argsCount);
		let pos=4;
		for(let i=0; i < argsCount; ++i){
			let type=dv.getUint8(pos++);
			switch(type){
				case _I:
					args[i]=dv.getInt32(pos);
					pos+=4;
					break;
				case _STRING:
					let len=dv.getInt32(pos);
					pos+=4;
					let strbytes = dv.buffer.slice(pos, pos + len);
					pos+=len;
					args[i] = new TextDecoder("utf-8").decode(strbytes);
					break;
			}

		}
		let handler=this.handlers[id];
		await handler.handler.apply(null, args);
	}

	registerHandler(id, type, argTypes){
		var that=this;
		if(argTypes===undefined){
			const arrBuf=new ArrayBuffer(7);
			let dv=new DataView(arrBuf);
			return async function(){
				const waiterId = ++that.wIDCounter;
				dv.setInt8(0, INVOKE_HANDLER);
				dv.setInt8(1, type);
				dv.setInt8(2, id);
				dv.setInt32(3, waiterId);
				return new Promise(function(resolve, reject){
					that.waiters[waiterId]=resolve;
					that.ws.send(arrBuf);
				});

			};
		}
		switch(type){
			case GENERAL:
				return async function(){
					const waiterId = ++that.wIDCounter;
					let buf=new WriteAble();
					buf.writeByte(INVOKE_HANDLER);
					buf.writeByte(type);
					buf.writeByte(id);
					buf.writeInt(waiterId);
					buf.writeByte(argTypes.length);
					for(let i=0; i<argTypes.length;++i){
						switch(argTypes[i]){
							case _STRING:
								buf.writeByte(_STRING);
								buf.writeString(arguments[i]);
								break;
							case _I:
								buf.writeByte(_I);
								buf.writeInt(arguments[i]);
								break;
							default:
								throw new Error("unsupported argument type id: "+argTypes[i]);

						}
					}
					let arrBuf=buf.toArray();
					return new Promise(function(resolve, reject){
						that.waiters[waiterId]=resolve;
						that.ws.send(arrBuf);
					});
				};
			case CONSUMER:
				return async function(){
					const waiterId = ++that.wIDCounter;
					let buf=new WriteAble();
					buf.writeByte(INVOKE_HANDLER);
					buf.writeByte(type);
					buf.writeByte(id);
					buf.writeInt(waiterId);
					for(let i=0; i<argTypes.length;++i){
						switch(argTypes[i]){
							case _STRING:
								buf.writeByte(_STRING);
								buf.writeString(arguments[i]);
								break;
							case _I:
								buf.writeByte(_I);
								buf.writeInt(arguments[i]);
								break;
							default:
								throw new Error("unsupported argument type id: "+argTypes[i]);

						}
					}
					let arrBuf=buf.toArray();
					return new Promise(function(resolve, reject){
						that.waiters[waiterId]=resolve;
						that.ws.send(arrBuf);
					});
				};
		}
	}

	processResponse(dv){
		let rid=dv.getInt32(1);
		let resolve=this.waiters[rid];
		if(resolve===undefined)
			throw new Error("wrong response id");
		delete this.waiters[rid];
		let pos=5;
		let type=dv.getInt8(pos++);
		switch(type){
			case _STRING:
				let strbytes = dv.buffer.slice(6, dv.buffer.byteLength);
				let str = new TextDecoder("utf-8").decode(strbytes);
				resolve(str);
				return;

		}
		resolve();
	}
}
