package com.jsrmi.ws;

public class User {
	private final String id;

	public User(String userID) {
		this.id=userID;
	}

	public String getID() {
		return id;
	}
}
