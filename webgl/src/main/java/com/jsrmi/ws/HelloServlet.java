package com.jsrmi.ws;


import jakarta.servlet.http.HttpServlet;
import jakarta.servlet.http.HttpServletRequest;
import jakarta.servlet.http.HttpServletResponse;

import java.io.*;

public class HelloServlet extends HttpServlet {
  private final String head;
  private final String body;

  public HelloServlet() {
    try {
      head = readString("/static/head.html");
      body = readString("/static/body.html");
    } catch (IOException e) {
      throw new RuntimeException(e);
    }
  }

  private String readString(String path) throws IOException {
    InputStream res = HelloServlet.class.getResourceAsStream(path);
    if (res != null) {
      return readString(res);
    }
    try (FileInputStream fis = new FileInputStream(path)) {
      return readString(fis);
    }
  }

  public String readString(InputStream is) throws IOException {
    StringBuilder sb = new StringBuilder();
    BufferedReader br = new BufferedReader(new InputStreamReader(is));
    String line;
    while ((line = br.readLine()) != null) {
      sb.append(line);
    }
    return sb.toString();
  }

  @Override
  protected void doGet(HttpServletRequest req, HttpServletResponse resp)
      throws IOException {
    PrintWriter out = resp.getWriter();
    out.write("<!DOCTYPE html>");
    out.write("<html>");
    out.write("<head>");
    out.write("<script>");
    out.write("var room=\"");
    out.write(req.getRequestURI());
    out.write("\";");
    out.write("</script>");
    out.write(head);
    out.write("</head>");
    out.write("<body>");
    out.write(body);
    out.write("</body>");
    out.write("</html>");
  }
}
