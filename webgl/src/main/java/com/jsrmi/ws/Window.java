package com.jsrmi.ws;

public interface Window {
	@Remote(id=1)
	void alert(String message);
	@Remote(id=2)
	boolean confirm(String message);
	@Remote(id=4)
	String prompt(String message);
}
