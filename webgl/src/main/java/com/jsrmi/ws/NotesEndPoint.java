package com.jsrmi.ws;


import java.util.concurrent.ConcurrentHashMap;

public class NotesEndPoint {
	private final Window window;
	private final ConcurrentHashMap<RMISocket, Window> sessions;
	private final NotesRoom workspace;

	public NotesEndPoint( NotesRoom workspace, Window window, ConcurrentHashMap<RMISocket, Window> sessions) {
		this.workspace=workspace;
		this.window=window;
		this.sessions=sessions;
	}
}
