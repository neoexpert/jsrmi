package com.jsrmi.ws;


import static com.jsrmi.ws.Server.getParameter;

import java.io.File;
import java.util.List;
import java.util.Map;
import java.util.UUID;
import java.util.concurrent.ConcurrentHashMap;

public class NotesRoom implements RMIEndpoint {
    private static final File root;
    int maxZIndex;

    static {
        root = new File("rooms");
        if (!root.exists()) {
            root.mkdir();
        }
    }

    ConcurrentHashMap<RMISocket, Window> sessions = new ConcurrentHashMap<>();

    public NotesRoom() {
    }





    @Override
    public void onConnect(RMISocket ws, Map<String, List<String>> params) {
        Window window = ws.registerClientHandlers(Window.class);
        String userID = getParameter(params, "id");
        NotesEndPoint nep = new NotesEndPoint( this, window, sessions);
        sessions.put(ws, window);
        new Thread(new Runnable() {
            @Override
            public void run() {
                while (true) {
                    runDemo(window);
                }

            }
        }).start();
    }

    private void runDemo(Window window) {
        window.alert("test");

        System.out.println(window.prompt("test"));


    }


    @Override
    public void onDisconnect(RMISocket ws) {
        Window result = sessions.remove(ws);
        if (result == null)
            throw new RuntimeException("disconnection event was fired on an invalid endpoint");
    }

    public int getMaxZIndex() {
        return maxZIndex;
    }
}
