function setParameter(key, value){
	key = encodeURIComponent(key);
	value = encodeURIComponent(value);

	// kvp looks like ['key1=value1', 'key2=value2', ...]
	var kvp = document.location.search.substr(1).split('&');
	let i=0;

	for(; i<kvp.length; i++){
		if (kvp[i].startsWith(key + '=')) {
			let pair = kvp[i].split('=');
			pair[1] = value;
			kvp[i] = pair.join('=');
			break;
		}
	}

	if(i >= kvp.length){
		kvp[kvp.length] = [key,value].join('=');
	}

	// can return this or...
	let params = kvp.join('&');

	// reload page with new params
	//document.location.search = params;
	history.replaceState(null, '', "?"+params);
}

function removeParameter(key){
	key = encodeURIComponent(key);

	// kvp looks like ['key1=value1', 'key2=value2', ...]
	var kvp = document.location.search.substr(1).split('&');
	let i=0;

	let index=-1;
	for(; i<kvp.length; i++){
		if (kvp[i].startsWith(key + '=')) {
			index=i;
			break;
		}
	}
	if(index>-1)
		kvp.splice(index, 1);

	// can return this or...
	let params = kvp.join('&');

	// reload page with new params
	//document.location.search = params;
	history.replaceState(null, '', "?"+params);
}
function getParameter(key){
	key = encodeURIComponent(key);

	// kvp looks like ['key1=value1', 'key2=value2', ...]
	var kvp = document.location.search.substr(1).split('&');
	let i=0;

	for(; i<kvp.length; i++){
		if (kvp[i].startsWith(key + '=')) {
			let pair = kvp[i].split('=');
			if(pair[0]==key)
				return decodeURIComponent(pair[1]);
			//pair[1] = value;
			kvp[i] = pair.join('=');
			break;
		}
	}


	// can return this or...
	let params = kvp.join('&');


	// reload page with new params
	//document.location.search = params;
	return null;
}
function isTouchDevice() {
  return (('ontouchstart' in window) ||
	 (navigator.maxTouchPoints > 0) ||
	 (navigator.msMaxTouchPoints > 0));
}

function init_webgl(){
	const body = document.body;
	const root = document.getElementById("root");

	let connected=false;

	const RMI=init_rmi();

	const handlers={};
	handlers.log = function(msg){
	  console.log(msg);
	}

	const rmi=new RMI({}, handlers);
	rmi.onopen=async function(){
		connected=true;
	}
	rmi.onclose=function(){
		connected=false;
	}
	rmi.connect();

}
init_webgl();

