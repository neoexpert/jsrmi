package com.jsrmi.ws;
import com.jsrmi.sender.RMIHandler;

import java.lang.reflect.InvocationHandler;
import java.lang.reflect.Method;
import java.util.HashMap;

public class RMIProxyInvocationHandler implements InvocationHandler {
	private final HashMap<Method, RMIHandler> handlers;

	public RMIProxyInvocationHandler(HashMap<Method, RMIHandler> handlers){
		this.handlers=handlers;
	}
	@Override
	public Object invoke(Object o, Method method, Object[] objects) throws Throwable {
		RMIHandler handler = handlers.get(method);
		if(handler==null)
			throw new RMIException("no handler found for method: "+method);
		return handler.invoke(objects);
	}
}
