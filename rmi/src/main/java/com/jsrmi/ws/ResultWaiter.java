package com.jsrmi.ws;

import java.nio.ByteBuffer;

public class ResultWaiter {
	public volatile ByteBuffer result;
	public volatile boolean exception;
	private volatile boolean completed;
	public boolean isCompleted() {
		return completed;
	}

	public void setResult(ByteBuffer buf) {
		completed=true;
		result=buf;
	}
}
