package com.jsrmi.ws;

import com.jsrmi.RemoteException;
import com.jsrmi.sender.RMIHandler;
import com.jsrmi.server.ConsumerHandler;
import com.jsrmi.server.ServerHandler;
import com.jsrmi.server.SupplierHanlder;
import java.io.IOException;
import java.lang.invoke.MethodHandle;
import java.lang.invoke.MethodHandles;
import java.lang.invoke.MethodType;
import java.lang.reflect.Method;
import java.lang.reflect.Proxy;
import java.nio.ByteBuffer;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.concurrent.ConcurrentHashMap;
import java.util.concurrent.atomic.AtomicInteger;
import java.util.function.Consumer;
import java.util.function.Supplier;

public class RMISocket {
  public static final byte EXCEPTION_RESPONSE = 0;
  public static final byte RESPONSE = 1;
  public static final byte REGISTER_HANDLER = 2;
  public static final byte REGISTER_ASYNC_HANDLER = 3;
  public static final byte INVOKE_HANDLER = 4;
  public static final byte INVOKE_HANDLER_ASYNC = 5;
  private final Map<String, List<String>> params;
  private AtomicInteger ridCounter = new AtomicInteger(0);
  ConcurrentHashMap<Integer, ResultWaiter> waiters = new ConcurrentHashMap<>();
  private RMISession session;
  private RMIEndpoint rmiEndPoint;

  public RMISocket(Map<String, List<String>> params) {
    this.params = params;
  }

  public RMISocket() {
    this(null);
  }

  public <T extends Object> T registerClientHandlers(Class<T> clazz) {
    Method[] methods = clazz.getMethods();
    HashMap<Method, RMIHandler> handlers = new HashMap<>();
    for (int i = 0; i < methods.length; i++) {
      Method m = methods[i];
      Class<?>[] types = m.getParameterTypes();
      byte[] rawtypes = Types.toBytes(types);
      byte rtype = Types.toByte(m.getReturnType());
      boolean async = m.getAnnotation(Async.class) != null;
      Remote remoteAnntation = m.getAnnotation(Remote.class);
        if (remoteAnntation == null) {
            throw new RMIException("method is not annotated with @Remote: " + m);
        }
      byte id = remoteAnntation.id();
      RMIHandler handler = registerHandler(id, m.getName(), rawtypes, rtype, async);
      handlers.put(m, handler);
    }
    return (T) Proxy.newProxyInstance(getClass().getClassLoader(), new Class[] {clazz},
        new RMIProxyInvocationHandler(handlers));
  }

  public void onConnect(RMISession session) {
    this.session = session;
    rmiEndPoint.onConnect(this, params);
  }

  public void onError(Throwable error) {
    error.printStackTrace();
    session.close();
  }

  public RMIHandler registerHandler(byte id, String methodName, byte[] ptypes, byte rtype,
                                    boolean async) {
    byte[] nameBytes = methodName.getBytes();
    ByteBuffer buffer = ByteBuffer.allocate(5 + nameBytes.length + ptypes.length);
    //1 byte
      if (async) {
          buffer.put(REGISTER_ASYNC_HANDLER);
      } else {
          buffer.put(REGISTER_HANDLER);
      }
    //1 byte
    buffer.put(id);
    //1 byte
    buffer.put(rtype);
    //1 byte
    buffer.put((byte) nameBytes.length);
    buffer.put(nameBytes);
    //1 byte
    buffer.put((byte) ptypes.length);
    buffer.put(ptypes);
    buffer.flip();
    send(buffer);

    return RMIHandler.find(this, id, rtype, ptypes, async);
  }

  public void send(byte[] params) {
    send(ByteBuffer.wrap(params));
  }

  public void send(ByteBuffer buffer) {
    try {
      session.sendBytes(buffer);
    } catch (IOException e) {
      throw new RuntimeException(e);
    }
  }

  public ByteBuffer invokeOnClient(ByteBuffer buffer) {
    int rid = ridCounter.incrementAndGet();
    ByteBuffer _buffer = ByteBuffer.allocate(5 + buffer.capacity());
    _buffer.put(INVOKE_HANDLER);
    _buffer.putInt(rid);
    _buffer.put(buffer);
    _buffer.flip();
    return invokeBlocking(rid, _buffer);
  }

  public void invokeRunnable(byte id) {
    int rid = ridCounter.incrementAndGet();
    ByteBuffer buffer = ByteBuffer.allocate(6);
    buffer.put(INVOKE_HANDLER);
    buffer.putInt(rid);
    buffer.put(id);
    buffer.flip();
    invokeBlocking(rid, buffer);
  }

  public ByteBuffer invokeBlocking(int rid, ByteBuffer buffer) {
    ResultWaiter waiter = new ResultWaiter();
      if (waiters.containsKey(rid)) {
          throw new RMIException("waiter collision");
      }
    waiters.put(rid, waiter);
    synchronized (waiter) {
      try {
        session.sendBytes(buffer);
      } catch (IOException e) {
        throw new RuntimeException(e);
      }
      try {
        while (!waiter.isCompleted()) {
          waiter.wait();
            if (closed) {
                throw new RMIException("socket closed");
            }
        }
      } catch (InterruptedException e) {
        throw new RuntimeException(e);
      }
    }
    if (waiter.exception) {
      byte[] arr = new byte[waiter.result.capacity() - waiter.result.position()];
      waiter.result.get(arr);
      String message = new String(arr);
      RemoteException exception = new RemoteException(message);
      throw exception;
    }
    return waiter.result;
  }

  public void onMessage(byte[] buf, int offset, int length) {
    ByteBuffer buffer = ByteBuffer.wrap(buf, offset, length);
    onMessage(buffer);
  }

  public void onMessage(ByteBuffer buffer) {
    byte cmd = buffer.get();
    switch (cmd) {
      case EXCEPTION_RESPONSE:
        int rid = buffer.getInt();
        ResultWaiter waiter = waiters.remove(rid);
          if (waiter == null) {
              throw new RMIException(
                  String.format("waiter (id = %d) was not found in the waiter map", rid));
          }
        waiter.exception = true;
        synchronized (waiter) {
          waiter.setResult(buffer);
          waiter.notify();
        }
        break;
      case RESPONSE:
        rid = buffer.getInt();
        waiter = waiters.remove(rid);
          if (waiter == null) {
              throw new RMIException(
                  String.format("waiter (id = %d) was not found in the waiter map", rid));
          }
        waiter.result = buffer;
        synchronized (waiter) {
          waiter.setResult(buffer);
          waiter.notify();
        }
        break;
      case INVOKE_HANDLER:
        invokeOnServer(session, buffer);
        break;
      case REGISTER_HANDLER:
        registerHandler(buffer);
        break;
      default:
        throw new RMIException("unknown command id:" + cmd);
    }
  }

  private void registerHandler(ByteBuffer buffer) {
    byte handlerID = buffer.get();
    byte rtype = buffer.get();
    byte length = buffer.get();
    byte[] nameBytes = new byte[length];
    buffer.get(nameBytes);
    String name = new String(nameBytes);
    byte argsLength = buffer.get();
    byte[] args = new byte[argsLength];
    buffer.get(args);
    ServerHandler handler = serverHandlers[handlerID];
    if (handler == null) {
      handler = rmiEndPoint.registerHandler(rtype, args, name);
      serverHandlers[handlerID] = handler;
    }
    handler.validate(rtype, args);
  }


  public void registerServerSupplier(byte id, Supplier<Object> supplier, byte rtype) {
    serverHandlers[id] = new SupplierHanlder(rtype, supplier);
  }

  public <T> void registerServerConsumer(byte id, Consumer<T> consumer, byte type) {
    serverHandlers[id] = new ConsumerHandler(type, consumer);
  }

  private void invokeOnServer(RMISession session, ByteBuffer buffer) {
    int rid = buffer.getInt();
    byte id = buffer.get();
    ServerHandler handler = serverHandlers[id];
    if (handler == null) {
      String msg = "no such handler: " + id;
      byte[] bytes = msg.getBytes();
      buffer = ByteBuffer.allocate(5 + 4 + bytes.length);
      buffer.put(EXCEPTION_RESPONSE);
      buffer.putInt(rid);
      buffer.putInt(bytes.length);
      buffer.put(bytes);
      buffer.flip();
      try {
        session.sendBytes(buffer);
      } catch (IOException ie) {
        throw new RuntimeException(ie);
      }
      return;
    }
    handler.process(rid, session, buffer);
  }

  private volatile boolean closed = false;

  public void onClose(RMISession session) {
    closed = true;
    rmiEndPoint.onDisconnect(this);
    for (ResultWaiter waiter : waiters.values()) {
      synchronized (waiter) {
        waiter.notify();
      }
    }
  }

  public void registerServerHandlers(Object obj) {
    Class<?> clazz = obj.getClass();
    for (Method m : clazz.getMethods()) {
      Remote remote = m.getAnnotation(Remote.class);
        if (remote != null) {
            registerServerHandler(remote.id(), clazz, obj, m);
        }
    }
  }

  //HashMap<Byte, ServerHandler> serverHandlers=new HashMap<>();
  final ServerHandler[] serverHandlers = new ServerHandler[256];

  private void registerServerHandler(byte id, Class clazz, Object obj, Method m) {
    MethodHandles.Lookup lookup = MethodHandles.lookup();
    Class<?> rtype = m.getReturnType();
    Class<?>[] paramTypes = m.getParameterTypes();
    MethodType mt = MethodType.methodType(rtype, paramTypes);
    MethodHandle mh;
    try {
      mh = lookup.findVirtual(clazz, m.getName(), mt);
    } catch (Throwable e) {
      throw new RuntimeException(e);
    }
    byte _rtype = Types.toByte(rtype);
    byte[] ptypes = Types.toBytes(paramTypes);
    serverHandlers[id] = ServerHandler.find(_rtype, ptypes, obj, mh);
  }


  public void invokeAsyncOnClient(ByteBuffer buffer) {
    ByteBuffer _buffer = ByteBuffer.allocate(1 + buffer.capacity());
    _buffer.put(INVOKE_HANDLER_ASYNC);
    _buffer.put(buffer);
    _buffer.flip();
    try {
      session.sendBytes(_buffer);
    } catch (IOException e) {
      throw new RuntimeException(e);
    }
  }

  public void close() {
    session.close();
  }

  public void setRMIEndPoint(RMIEndpoint rmiEndpoint) {
    this.rmiEndPoint = rmiEndpoint;
  }
}
