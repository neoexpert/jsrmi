package com.jsrmi.ws;

public interface Types {
	byte VOID = 0;
	byte BOOLEAN=1;
	byte INT8=2;
	byte INT16=3;
	byte UINT16=4;
	byte INT32=5;
	byte INT64=6;
	byte FLOAT32=7;
	byte FLOAT64=8;
	byte STRING = 9;

	byte BOOLEAN_ARRAY=10;
	byte INT8_ARRAY=11;
	byte INT16_ARRAY=12;
	byte UINT16_ARRAY=13;
	byte INT32_ARRAY=14;
	byte INT64_ARRAY=15;
	byte FLOAT32_ARRAY=16;
	byte FLOAT64_ARRAY=17;
	byte STRING_ARRAY = 18;

    static byte toByte(Class c){
		return toByte(c.getName());
	}
	static byte toByte(String c){
		switch (c){
			case "void":
				return VOID;
			case "boolean":
				return BOOLEAN;
			case "byte":
				return INT8;
			case "short":
				return INT16;
			case "char":
				return UINT16;
			case "int":
				return INT32;
			case "long":
				return INT64;
			case "float":
				return FLOAT32;
			case "double":
				return FLOAT64;
			case "java.lang.String":
				return STRING;
			case "[Ljava.lang.String;":
				return STRING_ARRAY;
			case "[Z":
				return BOOLEAN_ARRAY;
			case "[B":
				return INT8_ARRAY;
			case "[S":
				return INT16_ARRAY;
			case "[C":
				return UINT16_ARRAY;
			case "[I":
				return INT32_ARRAY;
			case "[J":
				return INT64_ARRAY;
			case "[F":
				return FLOAT32_ARRAY;
			case "[D":
				return FLOAT64_ARRAY;
			default:
				throw new RMIException("unsupported type: %s", c);
		}
	}

	static byte[] toBytes(Class[] types) {
		byte[] arr=new byte[types.length];
		for (int i=0;i<arr.length;++i){
			arr[i]=toByte(types[i]);
		}
		return arr;
	}

	static String toString(byte[] types){
			StringBuilder sb=new StringBuilder();
			String prefix="";
			for(byte type:types){
				sb.append(prefix);
				prefix=", ";
				sb.append(toString(type));
			}
			return sb.toString();
		}

	static String toString(byte type){
			switch (type){
				case VOID:
					return "void";
				case BOOLEAN:
					return "boolean";
				case INT8:
					return "byte";
				case INT16:
					return "short";
				case UINT16:
					return "char";
				case INT32:
					return "int";
				case INT64:
					return "long";
				case FLOAT32:
					return "float";
				case FLOAT64:
					return "double";
				case STRING:
					return "java.lang.String";
				case BOOLEAN_ARRAY:
					return "boolean[]";
				case INT8_ARRAY:
					return "byte[]";
				case INT16_ARRAY:
					return "short[]";
				case UINT16_ARRAY:
					return "char[]";
				case INT32_ARRAY:
					return "int[]";
				case INT64_ARRAY:
					return "long[]";
				case FLOAT32_ARRAY:
					return "float[]";
				case FLOAT64_ARRAY:
					return "double[]";
				case STRING_ARRAY:
					return "java.lang.String[]";
				default:
					throw new RMIException("unsupported type id: %d", type);
			}
	}
	static int size(byte type){
			switch (type){
				case VOID:
					return 0;
				case BOOLEAN:
				case INT8:
					return 1;
				case INT16:
				case UINT16:
					return 2;
				case INT32:
				case FLOAT32:
					return 4;
				case INT64:
				case FLOAT64:
					return 8;
				case STRING:
				case BOOLEAN_ARRAY:
				case INT8_ARRAY:
				case INT16_ARRAY:
				case UINT16_ARRAY:
				case INT32_ARRAY:
				case INT64_ARRAY:
				case FLOAT32_ARRAY:
				case FLOAT64_ARRAY:
				case STRING_ARRAY:
					return -1;
				default:
					throw new RMIException("unsupported type id: %d", type);
			}
	}
}
