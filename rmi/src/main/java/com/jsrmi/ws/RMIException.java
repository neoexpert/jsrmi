package com.jsrmi.ws;

public class RMIException extends RuntimeException {
	public RMIException(String message) {
		super(message, null, false, false);
	}

	public RMIException(String format, Object ... args) {
		super(String.format(format, args), null, false, false);
	}

	public RMIException() {
		super(null, null, false, false);

	}
}
