package com.jsrmi.ws;

import java.io.IOException;
import java.nio.ByteBuffer;

public abstract class RMISession {

	public abstract void sendBytes(ByteBuffer buffer) throws IOException;

	public abstract void close();
}
