package com.jsrmi.ws;

import com.jsrmi.server.ServerHandler;

import java.util.List;
import java.util.Map;

public interface RMIEndpoint {
	void onConnect(RMISocket rmiWebSocket, Map<String, List<String>> params);
	void onDisconnect(RMISocket rmiWebSocket);
	default ServerHandler registerHandler(byte rtype, byte[] paramTypes, String name){
		throw new RMIException("registerHandler is not implemented");
	}
}
