package com.jsrmi;

import com.jsrmi.ws.RMIException;

public class RemoteException extends RMIException {
	public RemoteException(String message) {
		super(message);
	}
}
