package com.jsrmi.server;

import com.jsrmi.ws.RMIException;
import com.jsrmi.ws.RMISession;

import java.io.IOException;
import java.nio.ByteBuffer;
import java.util.Arrays;
import java.util.BitSet;

import static com.jsrmi.ws.RMISocket.*;
import static com.jsrmi.ws.Types.*;

import com.jsrmi.ws.Types;

import java.lang.invoke.MethodHandle;

public abstract class ServerHandler {
    private final byte rtype;
    private final byte[] args;
    private final int argsCount;

    public ServerHandler(byte rtype, byte[] args) {
        this.argsCount = args.length;
        this.rtype = rtype;
        this.args = args;
    }

    public void process(int rid, RMISession session, ByteBuffer buffer) {
        Object[] args = new Object[argsCount + 1];
        //args[0]=obj;
        for (int i = 0; i < argsCount; ++i) {
            byte argType = this.args[i];
            switch (argType) {
                case BOOLEAN:
                    args[i + 1] = buffer.get() == 1;
                    break;
                case INT8:
                    args[i + 1] = buffer.get();
                    break;
                case INT16:
                    args[i + 1] = buffer.getShort();
                    break;
                case UINT16:
                    args[i + 1] = buffer.getChar();
                    break;
                case INT32:
                    args[i + 1] = buffer.getInt();
                    break;
                case INT64:
                    args[i + 1] = buffer.getLong();
                    break;
                case FLOAT32:
                    args[i + 1] = buffer.getFloat();
                    break;
                case FLOAT64:
                    args[i + 1] = buffer.getDouble();
                    break;
                case STRING:
                    int length = buffer.getInt();
                    byte[] buf = new byte[length];
                    buffer.get(buf);
                    args[i + 1] = new String(buf);
                    break;
                case BOOLEAN_ARRAY:
                    length = buffer.getInt();
                    buf = new byte[length];
                    buffer.get(buf);
                    boolean[] zarr = new boolean[length];
                    for (int j = 0; j < length; ++j)
                        zarr[j] = buf[j] == 1;
                    args[i + 1] = zarr;
                    break;
                case INT8_ARRAY:
                    length = buffer.getInt();
                    buf = new byte[length];
                    buffer.get(buf);
                    args[i + 1] = buf;
                    break;
                case INT16_ARRAY:
                    length = buffer.getInt();
                    short[] sarr = new short[length];
                    for (int j = 0; j < length; ++j)
                        sarr[j] = buffer.getShort();
                    args[i + 1] = sarr;
                    break;
                case UINT16_ARRAY:
                    length = buffer.getInt();
                    char[] carr = new char[length];
                    for (int j = 0; j < length; ++j)
                        carr[j] = buffer.getChar();
                    args[i + 1] = carr;
                    break;
                case INT32_ARRAY:
                    length = buffer.getInt();
                    int[] iarr = new int[length];
                    for (int j = 0; j < length; ++j)
                        iarr[j] = buffer.getInt();
                    args[i + 1] = iarr;
                    break;
                case INT64_ARRAY:
                    length = buffer.getInt();
                    long[] jarr = new long[length];
                    for (int j = 0; j < length; ++j)
                        jarr[j] = buffer.getLong();
                    args[i + 1] = jarr;
                    break;
                case FLOAT32_ARRAY:
                    length = buffer.getInt();
                    float[] farr = new float[length];
                    for (int j = 0; j < length; ++j)
                        farr[j] = buffer.getFloat();
                    args[i + 1] = farr;
                    break;
                case FLOAT64_ARRAY:
                    length = buffer.getInt();
                    double[] darr = new double[length];
                    for (int j = 0; j < length; ++j)
                        darr[j] = buffer.getDouble();
                    args[i + 1] = darr;
                    break;
                case STRING_ARRAY:
                    length = buffer.getInt();
                    String[] _sarr = new String[length];
                    for (int j = 0; j < length; ++j) {
                        int strLen = buffer.getInt();
                        byte[] strBytes = new byte[strLen];
                        buffer.get(strBytes);
                        _sarr[j] = new String(strBytes);
                    }
                    args[i + 1] = _sarr;
                    break;
                default:
                    throw new RuntimeException("unsupported type id: " + argType);
            }
        }
        Object o;
        try {
            o = invoke(args);
        } catch (Throwable e) {
            e.printStackTrace();
            String msg = e.getMessage() + "";
            byte[] bytes = msg.getBytes();
            buffer = ByteBuffer.allocate(5 + 4 + bytes.length);
            buffer.put(EXCEPTION_RESPONSE);
            buffer.putInt(rid);
            buffer.putInt(bytes.length);
            buffer.put(bytes);
            buffer.flip();
            try {
                session.sendBytes(buffer);
            } catch (IOException ie) {
                throw new RuntimeException(e);
            }
            return;
        }
        switch (rtype) {
            case BOOLEAN:
                buffer = ByteBuffer.allocate(5 + 1);
                buffer.put(RESPONSE);
                buffer.putInt(rid);
                buffer.put((byte) (o == Boolean.TRUE ? 1 : 0));
                break;
            case INT8:
                buffer = ByteBuffer.allocate(5 + 1);
                buffer.put(RESPONSE);
                buffer.putInt(rid);
                buffer.put((byte) (o));
                break;
            case INT16:
                buffer = ByteBuffer.allocate(5 + 2);
                buffer.put(RESPONSE);
                buffer.putInt(rid);
                buffer.putShort((short) o);
                break;
            case UINT16:
                buffer = ByteBuffer.allocate(5 + 2);
                buffer.put(RESPONSE);
                buffer.putInt(rid);
                buffer.putChar((char) o);
                break;
            case INT32:
                buffer = ByteBuffer.allocate(5 + 4);
                buffer.put(RESPONSE);
                buffer.putInt(rid);
                buffer.putInt((int) o);
                break;
            case INT64:
                buffer = ByteBuffer.allocate(5 + 8);
                buffer.put(RESPONSE);
                buffer.putInt(rid);
                buffer.putLong((long) o);
                break;
            case FLOAT32:
                buffer = ByteBuffer.allocate(5 + 4);
                buffer.put(RESPONSE);
                buffer.putInt(rid);
                buffer.putFloat((float) o);
                break;
            case FLOAT64:
                buffer = ByteBuffer.allocate(5 + 8);
                buffer.put(RESPONSE);
                buffer.putInt(rid);
                buffer.putDouble((double) o);
                break;
            case STRING:
                byte[] ret = ((String) o).getBytes();
                buffer = ByteBuffer.allocate(5 + ret.length);
                buffer.put(RESPONSE);
                buffer.putInt(rid);
                buffer.put(ret);
                break;
            case VOID:
                buffer = ByteBuffer.allocate(5);
                buffer.put(RESPONSE);
                buffer.putInt(rid);
                break;
            default:
                throw new RuntimeException("unsuppored type: " + o.getClass().getName());
        }
        buffer.flip();
        try {
            session.sendBytes(buffer);
        } catch (IOException e) {
            throw new RuntimeException(e);
        }
    }

    protected abstract Object invoke(Object[] args);

    public void validate(byte rtype, byte[] args) {
        if (rtype != this.rtype)
            throw new RMIException("handler vailidation failed: rtype!=this.rtype (%s!=%s)", Types.toString(rtype), Types.toString(this.rtype));
        if (!Arrays.equals(args, this.args))
            throw new RMIException("handler vailidation failed: args!=this.args (%s)!=(%s)", Types.toString(args), Types.toString(this.args));
    }

    public static ServerHandler find(byte rtype, byte[] ptypes, Object obj, MethodHandle mh) {
        if (rtype == VOID) {
            if (ptypes.length == 0)
                return new RunnableHandler(obj, mh);
            if (ptypes.length == 1) {
                switch (ptypes[0]) {
                    case BOOLEAN:
                        return new BooleanConsumerReceiver(obj, mh);
                    case INT8:
                        return new ByteConsumerReceiver(obj, mh);
                    case INT16:
                        return new ShortConsumerReceiver(obj, mh);
                    case UINT16:
                        return new CharConsumerReceiver(obj, mh);
                    case INT32:
                        return new IntConsumerHandler(obj, mh);
                    case INT64:
                        return new LongConsumerReceiver(obj, mh);
                    case FLOAT32:
                        return new FloatConsumerReceiver(obj, mh);
                    case FLOAT64:
                        return new DoubleConsumerReceiver(obj, mh);
                    case STRING:
                        return new StringConsumerReceiver(obj, mh);
                }
            }
        }
        return new MethodHandleHandler(rtype, ptypes, obj, mh);
    }
}
