package com.jsrmi.server;

import java.util.function.IntConsumer;

import static com.jsrmi.ws.Types.VOID;
import static com.jsrmi.ws.Types.INT8;
import com.jsrmi.ws.RMIException;
import com.jsrmi.ws.RMISession;

import java.io.IOException;
import java.nio.ByteBuffer;
import java.util.Arrays;
import java.util.BitSet;

import static com.jsrmi.ws.RMISocket.*;
import  com.jsrmi.ws.Types;
import java.lang.invoke.MethodHandle;


public class ByteConsumerReceiver extends ServerHandler{
	private static final byte[] PTYPES=new byte[]{INT8};

	private final Object obj;
	private final MethodHandle mh;
	public ByteConsumerReceiver(Object obj, MethodHandle mh) {
		super(VOID, PTYPES);
				this.obj=obj;
				this.mh=mh;
	}

	@Override
	protected Object invoke(Object[] args) {
		return null;
	}

	public void process(int rid, RMISession session, ByteBuffer buffer) {
		try {
					mh.invoke(obj, buffer.get());
		} catch (Throwable e) {
						String msg=e+"";
						byte[] bytes=msg.getBytes();
						buffer=ByteBuffer.allocate(5+4+bytes.length);
						buffer.put(EXCEPTION_RESPONSE);
						buffer.putInt(rid);
						buffer.putInt(bytes.length);
						buffer.put(bytes);
						buffer.flip();
						try {
							session.sendBytes(buffer);
						} catch (IOException ie) {
							throw new RuntimeException(e);
						}
						return;
		}
				buffer=ByteBuffer.allocate(5);
				buffer.put(RESPONSE);
				buffer.putInt(rid);
				buffer.flip();
				try {
					session.sendBytes(buffer);
				} catch (IOException e) {
					throw new RuntimeException(e);
				}
	}
}
