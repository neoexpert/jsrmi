package com.jsrmi.server;

import java.util.function.Consumer;

import static com.jsrmi.ws.Types.VOID;

public class ConsumerHandler<T> extends ServerHandler{
	private final Consumer<T> consumer;

	public ConsumerHandler(byte type, Consumer<T> consumer) {
		super(VOID, new byte[]{type});
		this.consumer=consumer;
	}

	@Override
	protected Object invoke(Object[] args) {
		consumer.accept((T) args[1]);
		return null;
	}
}
