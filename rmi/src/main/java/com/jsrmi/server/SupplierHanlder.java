package com.jsrmi.server;

import java.util.function.Supplier;

public class SupplierHanlder extends ServerHandler{
	private final Supplier<Object> supplier;

	public SupplierHanlder(byte rtype, Supplier<Object> supplier) {
		super(rtype, new byte[0]);
		this.supplier=supplier;
	}

	@Override
	protected Object invoke(Object[] args) {
		return supplier.get();
	}
}
