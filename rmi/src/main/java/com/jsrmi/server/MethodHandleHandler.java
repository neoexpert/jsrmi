package com.jsrmi.server;

import java.lang.invoke.MethodHandle;

public class MethodHandleHandler extends ServerHandler{
	public final Object obj;
	public final MethodHandle mh;
	public MethodHandleHandler(byte rtype, byte[] args, Object obj, MethodHandle mh) {
		super(rtype, args);
		this.obj=obj;
		this.mh=mh;
	}

	@Override
	protected Object invoke(Object[] args) {
		args[0]=obj;
		try {
			return mh.invokeWithArguments(args);
		} catch (Throwable e) {
			throw new RuntimeException(e);
		}
	}
}
