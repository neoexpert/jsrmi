package com.jsrmi.sender;
import com.jsrmi.ws.*;

import java.nio.ByteBuffer;

import static com.jsrmi.ws.Types.*;

public abstract class RMIHandler {
	protected final RMISocket ws;
	protected final byte id;
	protected final byte[] signature;
	protected final byte rtype;
	protected final boolean async;

	public RMIHandler(RMISocket ws, byte id, byte rtype,  byte[] signature, boolean async){
		this.ws=ws;
		this.id=id;
		this.signature=signature;
		this.rtype=rtype;
		this.async=async;
	}
	public abstract <T extends Object> T invoke(Object... params);

	protected  <T extends Object> T _return(ByteBuffer buffer) {
			switch (rtype) {
				case VOID:
					return null;
				case BOOLEAN:
					return (T) (buffer.get()==1?Boolean.TRUE:Boolean.FALSE);
				case INT8:
					return (T) (Byte)buffer.get();
				case INT16:
					return (T) (Short)buffer.getShort();
				case UINT16:
					return (T) (Character)buffer.getChar();
				case INT32:
					return (T) Integer.valueOf(buffer.getInt());
				case INT64:
					return (T) (Long)buffer.getLong();
				case FLOAT32:
					return (T) (Float)buffer.getFloat();
				case FLOAT64:
					return (T) (Double)buffer.getDouble();
				case STRING:
					int pos=buffer.position();
					if(pos==buffer.capacity())
						return (T) "";
					if(buffer.get()==0)
						return null;
					buffer.position(pos);
					byte[] arr=new byte[buffer.capacity() - buffer.position()];
					buffer.get(arr);
					return (T) new String(arr);
				default:
					throw new RMIException("unknown type id: " + rtype);
			}
	}

	protected  <T> T _invoke(ByteBuffer buffer) {
		if(async) {
			ws.invokeAsyncOnClient(buffer);
			return null;
		}
		else
			buffer=ws.invokeOnClient(buffer);
		return _return(buffer);
	}

	protected  void _invokeVOID(ByteBuffer buffer) {
		if(async)
			ws.invokeAsyncOnClient(buffer);
		else
			ws.invokeOnClient(buffer);
	}

		public static RMIHandler find(RMISocket s, byte id, byte rtype, byte[] ptypes, boolean async){
			switch(rtype){
				case VOID:
					if(ptypes.length==0)
						return new RunnableSender(s, id, async);
					if(ptypes.length==1){
						switch(ptypes[0]){
							case BOOLEAN:
								return new BooleanConsumerSender(s, id, async);
							case INT8:
								return new ByteConsumerSender(s, id, async);
							case INT16:
								return new ShortConsumerSender(s, id, async);
							case UINT16:
								return new CharConsumerSender(s, id, async);
							case INT32:
								return new IntConsumerSender(s, id, async);
							case INT64:
								return new LongConsumerSender(s, id, async);
							case FLOAT32:
								return new FloatConsumerSender(s, id, async);
							case FLOAT64:
								return new DoubleConsumerSender(s, id, async);
							case STRING:
								return new StringConsumerSender(s, id, async);
						}
					}
					break;
				default:
					if(ptypes.length==0){
						switch(rtype){
							case BOOLEAN:
								return new BooleanSupplierSender(s, id);
							case INT8:
								return new ByteSupplierSender(s, id);
							case INT16:
								return new ShortSupplierSender(s, id);
							case UINT16:
								return new CharSupplierSender(s, id);
							case INT32:
								return new IntSupplierSender(s, id);
							case INT64:
								return new LongSupplierSender(s, id);
							case FLOAT32:
								return new FloatSupplierSender(s, id);
							case FLOAT64:
								return new DoubleSupplierSender(s, id);
						}
						return new SupplierSender(s, id, rtype);
					}

			}
		int bufferSize=1;
				for(int i=0;i<ptypes.length;++i){
					byte type=ptypes[i];
					int size=Types.size(type);
					if(size==-1){
						bufferSize=-1;
						break;
					}
					bufferSize+=size;
				}

		if(bufferSize==-1)
			return new DynamicSizeRMIHandler(s, id, rtype, ptypes, async);
		else
			return new FixedSizeRMIHandler(s, id, bufferSize, rtype, ptypes, async);
		}
}
