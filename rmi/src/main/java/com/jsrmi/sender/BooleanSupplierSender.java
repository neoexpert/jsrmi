package com.jsrmi.sender;

import com.jsrmi.ws.*;

import java.nio.ByteBuffer;

import static com.jsrmi.ws.Types.*;

public class BooleanSupplierSender extends SupplierSender {

    public BooleanSupplierSender(RMISocket ws, byte id) {
        super(ws, id, BOOLEAN);
    }


    protected <T extends Object> T _return(ByteBuffer buffer) {
        return (T) (buffer.get() == 1 ? Boolean.TRUE : Boolean.FALSE);
    }

}
