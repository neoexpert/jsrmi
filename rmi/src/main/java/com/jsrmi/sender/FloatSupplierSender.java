package com.jsrmi.sender;
import com.jsrmi.ws.*;

import java.nio.ByteBuffer;

import static com.jsrmi.ws.Types.*;

public class FloatSupplierSender extends SupplierSender{

	public FloatSupplierSender(RMISocket ws, byte id) {
		super(ws, id, FLOAT32);
	}


	protected  <T extends Object> T _return(ByteBuffer buffer) {
					return (T) Float.valueOf(buffer.getFloat());
	}

}
