package com.jsrmi.sender;
import com.jsrmi.ws.*;

import java.nio.ByteBuffer;

import static com.jsrmi.ws.Types.*;

public class LongSupplierSender extends SupplierSender{

	public LongSupplierSender(RMISocket ws, byte id) {
		super(ws, id, INT64);
	}


	protected  <T extends Object> T _return(ByteBuffer buffer) {
					return (T) Long.valueOf(buffer.getLong());
	}

}
