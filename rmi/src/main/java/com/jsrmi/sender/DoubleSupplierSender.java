package com.jsrmi.sender;
import com.jsrmi.ws.*;

import java.nio.ByteBuffer;

import static com.jsrmi.ws.Types.*;

public class DoubleSupplierSender extends SupplierSender{

	public DoubleSupplierSender(RMISocket ws, byte id) {
		super(ws, id, FLOAT64);
	}


	protected  <T extends Object> T _return(ByteBuffer buffer) {
					return (T) Double.valueOf(buffer.getDouble());
	}

}
