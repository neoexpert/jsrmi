package com.jsrmi.sender;
import com.jsrmi.ws.*;

import java.nio.ByteBuffer;

import static com.jsrmi.ws.Types.*;

public class DoubleConsumerSender extends RMIHandler{
	private static final byte[] PTYPES=new byte[]{FLOAT64};

	public DoubleConsumerSender(RMISocket ws, byte id, boolean async) {
		super(ws, id, VOID, PTYPES, async);
	}

	public <T extends Object> T invoke(Object ... params){
		ByteBuffer buffer = ByteBuffer.allocate(9);
		buffer.put((byte) id);
				buffer.putDouble((double) params[0]);
		buffer.flip();
		_invokeVOID(buffer);
				return null;
	}

}
