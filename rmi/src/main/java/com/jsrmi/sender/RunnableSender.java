package com.jsrmi.sender;

import com.jsrmi.ws.*;

import java.nio.ByteBuffer;

import static com.jsrmi.ws.Types.*;

public class RunnableSender extends RMIHandler {
    private static final byte[] PTYPES = new byte[]{};

    public RunnableSender(RMISocket ws, byte id, boolean async) {
        super(ws, id, VOID, PTYPES, async);
    }

    public <T extends Object> T invoke(Object... params) {
        ws.invokeRunnable(id);
        return null;
    }
}
