package com.jsrmi.sender;

import com.jsrmi.ws.*;
import java.nio.ByteBuffer;
import java.util.function.Consumer;

import static com.jsrmi.ws.RMISocket.INVOKE_HANDLER;
import static com.jsrmi.ws.Types.*;

public class DynamicSizeRMIHandler extends RMIHandler {
	public DynamicSizeRMIHandler(RMISocket ws, byte id, byte rtype, byte[] signature, boolean async) {
		super(ws, id, rtype, signature, async);
	}

	@Override
	public <T> T invoke(Object... params) {
		Consumer<ByteBuffer> paramsConsumer[]=new Consumer[params.length];
		//calculate size
		int bufferSize=1;
		for(int i=0;i<params.length;++i){
			byte type=signature[i];
			switch (type){
				case BOOLEAN:
					bufferSize+=1;
					final boolean _z= (boolean) params[i];
					paramsConsumer[i] = bb -> {
						bb.put((byte)(_z?1:0));
					};
					break;
				case INT8:
					bufferSize+=1;
					final byte _b= (byte) params[i];
					paramsConsumer[i] = bb -> {
						bb.put(_b);
					};
					break;
				case INT16:
					bufferSize+=2;
					final short _s= (short) params[i];
					paramsConsumer[i] = bb -> {
						bb.putShort(_s);
					};
					break;
				case UINT16:
					bufferSize+=2;
					final char _c= (char) params[i];
					paramsConsumer[i] = bb -> {
						bb.putChar(_c);
					};
					break;
				case INT32:
					bufferSize+=4;
					final int _i= (int) params[i];
					paramsConsumer[i] = bb -> {
						bb.putInt(_i);
					};
					break;
				case INT64:
					bufferSize+=8;
					final long _j= (long) params[i];
					paramsConsumer[i] = bb -> {
						bb.putLong(_j);
					};
										break;
				case FLOAT32:
					bufferSize+=4;
					final float _f= (float) params[i];
					paramsConsumer[i] = bb -> {
						bb.putFloat(_f);
					};
										break;
				case FLOAT64:
					bufferSize+=8;
					final double _d= (double) params[i];
					paramsConsumer[i] = bb -> {
						bb.putDouble(_d);
					};
										break;
				case STRING:
					String s= (String) params[i];
					final byte[] strBytes = s.getBytes();
					bufferSize+=(4 + strBytes.length);
					paramsConsumer[i] = bb -> {
						bb.putInt(strBytes.length);
						bb.put(strBytes);
					};
					break;
				case BOOLEAN_ARRAY:
										boolean[] zarr=(boolean[])params[i];
					bufferSize+=(4 + zarr.length);
					paramsConsumer[i] = bb -> {
						bb.putInt(zarr.length);
												for(boolean z:zarr)
													bb.put((byte)(z==true?1:0));
					};
										break;
				case INT8_ARRAY:
										byte[] barr=(byte[])params[i];
					bufferSize+=(4 + barr.length);
					paramsConsumer[i] = bb -> {
						bb.putInt(barr.length);
												bb.put(barr);
					};
										break;
				case INT16_ARRAY:
										short[] sarr=(short[])params[i];
					bufferSize+=(4 + sarr.length*2);
					paramsConsumer[i] = bb -> {
						bb.putInt(sarr.length);
												for(short __s:sarr)
													bb.putShort(__s);
					};
										break;
				case UINT16_ARRAY:
										char[] carr=(char[])params[i];
					bufferSize+=(4 + carr.length*2);
					paramsConsumer[i] = bb -> {
						bb.putInt(carr.length);
												for(char c:carr)
													bb.putChar(c);
					};
										break;
				case INT32_ARRAY:
										int[] iarr=(int[])params[i];
					bufferSize+=(4 + iarr.length*4);
					paramsConsumer[i] = bb -> {
						bb.putInt(iarr.length);
												for(int __i:iarr)
													bb.putInt(__i);
					};
										break;
				case INT64_ARRAY:
										long[] jarr=(long[])params[i];
					bufferSize+=(4 + jarr.length*8);
					paramsConsumer[i] = bb -> {
						bb.putInt(jarr.length);
												for(long j:jarr)
													bb.putLong(j);
					};
										break;
				case FLOAT32_ARRAY:
										float[] farr=(float[])params[i];
					bufferSize+=(4 + farr.length*4);
					paramsConsumer[i] = bb -> {
						bb.putInt(farr.length);
												for(float f:farr)
													bb.putFloat(f);
					};
										break;
				case FLOAT64_ARRAY:
										double[] darr=(double[])params[i];
					bufferSize+=(4 + darr.length*8);
					paramsConsumer[i] = bb -> {
						bb.putInt(darr.length);
												for(double d:darr)
													bb.putDouble(d);
					};
										break;
				case STRING_ARRAY:
										String[] _sarr=(String[])params[i];
					bufferSize+=4;
										for(String __s:_sarr){
											byte[] _strBytes=__s.getBytes();
											bufferSize+=4;
											bufferSize+=_strBytes.length;;
										}
					paramsConsumer[i] = bb -> {
											bb.putInt(_sarr.length);
											for(String __s:_sarr){
												byte[] _strBytes=__s.getBytes();
												bb.putInt(_strBytes.length);
												bb.put(_strBytes);
											}
					};
										break;
								default:
										throw new RMIException("unsupported type id: %d", type);
			}
		}
		ByteBuffer buffer = ByteBuffer.allocate(bufferSize);
		buffer.put((byte) id);
		for(int i=0;i<paramsConsumer.length;++i){
			paramsConsumer[i].accept(buffer);
		}
		buffer.flip();
		return _invoke(buffer);
	}

}
