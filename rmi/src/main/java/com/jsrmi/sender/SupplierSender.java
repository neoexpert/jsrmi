package com.jsrmi.sender;
import com.jsrmi.ws.*;

import java.nio.ByteBuffer;

import static com.jsrmi.ws.Types.*;

public class SupplierSender extends RMIHandler{

	private static final byte[] PTYPES=new byte[]{};

	public SupplierSender(RMISocket ws, byte id, byte rtype) {
		super(ws, id, rtype, PTYPES, false);
	}

	protected  <T> T _invoke(ByteBuffer buffer) {
		buffer=ws.invokeOnClient(buffer);
		return _return(buffer);
	}

	public <T extends Object> T invoke(Object ... params){
		ByteBuffer buffer = ByteBuffer.allocate(1);
		buffer.put((byte) id);
		buffer.flip();
		return _invoke(buffer);
	}

}
