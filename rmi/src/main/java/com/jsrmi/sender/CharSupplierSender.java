package com.jsrmi.sender;
import com.jsrmi.ws.*;

import java.nio.ByteBuffer;

import static com.jsrmi.ws.Types.*;

public class CharSupplierSender extends SupplierSender{

	public CharSupplierSender(RMISocket ws, byte id) {
		super(ws, id, UINT16);
	}


	protected  <T extends Object> T _return(ByteBuffer buffer) {
					return (T) Character.valueOf(buffer.getChar());
	}

}
