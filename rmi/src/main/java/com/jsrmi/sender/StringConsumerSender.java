package com.jsrmi.sender;
import com.jsrmi.ws.*;

import java.nio.ByteBuffer;

import static com.jsrmi.ws.Types.*;

public class StringConsumerSender extends RMIHandler{
	private static final byte[] PTYPES=new byte[]{STRING};

	public StringConsumerSender(RMISocket ws, byte id, boolean async) {
		super(ws, id, VOID, PTYPES, async);
	}

		public <T extends Object> T invoke(Object ... params){
			String s=(String)params[0];
			if(s==null){
				ByteBuffer buffer = ByteBuffer.allocate(5);
				buffer.put((byte) id);
				buffer.putInt(-1);
				buffer.flip();
				_invokeVOID(buffer);
				return null;
			}
			byte[] bytes=s.getBytes();
			ByteBuffer buffer = ByteBuffer.allocate(5+bytes.length);
			buffer.put((byte) id);
			buffer.putInt(bytes.length);
			buffer.put(bytes);
			buffer.flip();
			 _invokeVOID(buffer);
			 return null;
		}

}
