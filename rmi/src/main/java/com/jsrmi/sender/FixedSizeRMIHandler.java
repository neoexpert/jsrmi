package com.jsrmi.sender;
import com.jsrmi.ws.*;

import java.nio.ByteBuffer;

import static com.jsrmi.ws.Types.*;

public class FixedSizeRMIHandler extends RMIHandler{
	private final int bufferSize;

	public FixedSizeRMIHandler(RMISocket ws, byte id, int bufferSize, byte rtype, byte[] signature, boolean async) {
		super(ws, id, rtype, signature, async);
		this.bufferSize=bufferSize;
	}

	public <T extends Object> T invoke(Object ... params){
		ByteBuffer buffer = ByteBuffer.allocate(bufferSize);
		buffer.put((byte) id);
		if(params!=null)
			for (int i = 0; i < params.length; ++i) {
				byte type = signature[i];
				switch (type) {
					case BOOLEAN:
						buffer.put((byte)(params[i]==Boolean.TRUE?1:0));
												break;
					case INT8:
						buffer.put((Byte) params[i]);
												break;
					case INT16:
						buffer.putShort((Short) params[i]);
												break;
					case UINT16:
						buffer.putChar((Character) params[i]);
												break;
					case INT32:
						Integer _i = (Integer) params[i];
						buffer.putInt(_i);
												break;
					case INT64:
						Long _j = (Long) params[i];
						buffer.putLong(_j);
												break;
					case FLOAT32:
						buffer.putFloat((Float) params[i]);
												break;
					case FLOAT64:
						buffer.putDouble((Double) params[i]);
												break;
										default:
												throw new RMIException("unsupported type id: %d", type);
				}
			}
		buffer.flip();
		return _invoke(buffer);
	}

}
