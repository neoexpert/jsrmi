package com.jsrmi.sender;

import com.jsrmi.ws.*;

import java.nio.ByteBuffer;

import static com.jsrmi.ws.Types.*;

public class IntConsumerSender extends RMIHandler {
    private static final byte[] PTYPES = new byte[]{INT32};

    public IntConsumerSender(RMISocket ws, byte id, boolean async) {
        super(ws, id, VOID, PTYPES, async);
    }

    public <T extends Object> T invoke(Object... params) {
        ByteBuffer buffer = ByteBuffer.allocate(5);
        buffer.put((byte) id);
        buffer.putInt((int) params[0]);
        buffer.flip();
        _invokeVOID(buffer);
        return null;
    }

}
