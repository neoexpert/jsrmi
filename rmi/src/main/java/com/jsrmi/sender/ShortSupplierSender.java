package com.jsrmi.sender;
import com.jsrmi.ws.*;

import java.nio.ByteBuffer;

import static com.jsrmi.ws.Types.*;

public class ShortSupplierSender extends SupplierSender{

	public ShortSupplierSender(RMISocket ws, byte id) {
		super(ws, id, INT16);
	}


	protected  <T extends Object> T _return(ByteBuffer buffer) {
					return (T) Short.valueOf(buffer.getShort());
	}

}
