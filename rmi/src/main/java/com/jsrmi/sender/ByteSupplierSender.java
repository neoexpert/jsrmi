package com.jsrmi.sender;
import com.jsrmi.ws.*;

import java.nio.ByteBuffer;

import static com.jsrmi.ws.Types.*;

public class ByteSupplierSender extends SupplierSender{

	public ByteSupplierSender(RMISocket ws, byte id) {
		super(ws, id, INT8);
	}


	protected  <T extends Object> T _return(ByteBuffer buffer) {
					return (T) Byte.valueOf(buffer.get());
	}

}
