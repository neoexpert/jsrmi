package com.jsrmi.sender;
import com.jsrmi.ws.*;

import java.nio.ByteBuffer;

import static com.jsrmi.ws.Types.*;

public class LongConsumerSender extends RMIHandler{
	private static final byte[] PTYPES=new byte[]{INT64};

	public LongConsumerSender(RMISocket ws, byte id, boolean async) {
		super(ws, id, VOID, PTYPES, async);
	}

	public <T extends Object> T invoke(Object ... params){
		ByteBuffer buffer = ByteBuffer.allocate(9);
		buffer.put((byte) id);
				buffer.putLong((long) params[0]);
		buffer.flip();
		_invokeVOID(buffer);
				return null;
	}

}
