package com.jsrmi.sender;
import com.jsrmi.ws.*;

import java.nio.ByteBuffer;

import static com.jsrmi.ws.Types.*;

public class IntSupplierSender extends SupplierSender{

	public IntSupplierSender(RMISocket ws, byte id) {
		super(ws, id, INT32);
	}


	protected  <T extends Object> T _return(ByteBuffer buffer) {
					return (T) Integer.valueOf(buffer.getInt());
	}

}
