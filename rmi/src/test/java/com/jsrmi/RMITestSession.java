package com.jsrmi;

import com.jsrmi.ws.RMISession;
import com.jsrmi.ws.RMISocket;

import java.io.IOException;
import java.nio.ByteBuffer;
import java.util.concurrent.LinkedBlockingQueue;

public class RMITestSession extends RMISession {
	private final RMISocket remoteSocket;
	private boolean closed;

	public RMITestSession(RMISocket ws){
		this.remoteSocket = ws;
	}

	public static ByteBuffer clone(ByteBuffer original) {
		ByteBuffer clone = ByteBuffer.allocate(original.capacity());
		original.rewind();//copy from the beginning
		clone.put(original);
		original.rewind();
		clone.flip();
		return clone;
	}

	@Override
	public void sendBytes(ByteBuffer buffer) throws IOException {
		if (buffer.capacity() != buffer.limit()) throw new RuntimeException("" + buffer);
		try {
			messages.put(buffer);
		} catch (InterruptedException e) {
			e.printStackTrace();
			throw new RuntimeException(e);
		}
	}
	LinkedBlockingQueue<ByteBuffer> messages=new LinkedBlockingQueue<>();

	Thread thread;
	@Override
	public void close() {
		closed=true;
		thread.interrupt();
	}

	public void open(){
		Thread thread=new Thread(new Runnable() {
			@Override
			public void run() {
				while (!closed){
					ByteBuffer message;
					try {
						message = messages.take();
						remoteSocket.onMessage(message);
					} catch (InterruptedException e) {
						if(closed)
							return;
					}
				}
			}
		});
		this.thread=thread;

		thread.start();
	}
}
