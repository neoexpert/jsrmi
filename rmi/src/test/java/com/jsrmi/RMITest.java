package com.jsrmi;

import com.jsrmi.ws.RMIEndpoint;
import com.jsrmi.ws.RMISocket;
import com.jsrmi.ws.Remote;
import org.junit.jupiter.api.Test;

import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import java.util.concurrent.atomic.AtomicInteger;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.fail;

public class RMITest implements RMIEndpoint {

	long testValue;
	AtomicInteger counter = new AtomicInteger();

	@Remote(id = 1)
	public void method() {
		counter.incrementAndGet();
		testValue = 1;
	}

	@Remote(id = 2)
	public void consumer(boolean value) {
		if (value)
			testValue = 1;
		else
			testValue = 0;
	}

	@Remote(id = 3)
	public void consumer(byte value) {
		testValue = value;
	}

	@Remote(id = 4)
	public void consumer(short value) {
		testValue = value;
	}

	@Remote(id = 5)
	public void consumer(char value) {
		testValue = value;
	}

	@Remote(id = 6)
	public void consumer(int value) {
		testValue = value;
	}

	@Remote(id = 7)
	public void consumer(long value) {
		testValue = value;
	}

	@Remote(id = 8)
	public void consumer(float value) {
		testValue = (long) value;
	}

	@Remote(id = 9)
	public void consumer(double value) {
		testValue = (long) value;
	}

	@Remote(id = 10)
	public void consumer(String value) {
		testValue = value.length();
	}

	@Remote(id = 11)
	public void consumer(boolean[] values) {
		testValue = 0;
		for (boolean b : values) {
			if (b)
				++testValue;
		}
	}

	@Remote(id = 12)
	public void consumer(byte[] values) {
		testValue = 0;
		for (byte b : values) {
			testValue += b;
		}
	}

	@Remote(id = 13)
	public void consumer(short[] values) {
		testValue = 0;
		for (short b : values) {
			testValue += b;
		}
	}

	@Remote(id = 14)
	public void consumer(char[] values) {
		testValue = 0;
		for (char b : values) {
			testValue += b;
		}
	}

	@Remote(id = 15)
	public void consumer(int[] values) {
		testValue = 0;
		for (int b : values) {
			testValue += b;
		}
	}

	@Remote(id = 16)
	public void consumer(long[] values) {
		testValue = 0;
		for (long b : values) {
			testValue += b;
		}
	}

	@Remote(id = 17)
	public void consumer(float[] values) {
		testValue = 0;
		for (float b : values) {
			testValue += b;
		}
	}

	@Remote(id = 18)
	public void consumer(double[] values) {
		testValue = 0;
		for (double b : values) {
			testValue += b;
		}
	}

	@Remote(id = 19)
	public void consumer(String[] values) {
		testValue = 0;
		for (String b : values) {
			testValue += b.length();
		}
	}

	@Remote(id = 20)
	public boolean booleanSupplier() {
		return testValue == 1L;
	}

	@Remote(id = 21)
	public byte byteSupplier() {
		return (byte) testValue;
	}

	@Remote(id = 22)
	public short shortSupplier() {
		return (short) testValue;
	}

	@Remote(id = 23)
	public char charSupplier() {
		return (char) testValue;
	}

	@Remote(id = 24)
	public int intSupplier() {
		return (int) testValue;
	}

	@Remote(id = 25)
	public long longSupplier() {
		return (int) testValue;
	}

	@Remote(id = 26)
	public float floatSupplier() {
		return (float) testValue;
	}

	@Remote(id = 27)
	public double doubleSupplier() {
		return (double) testValue;
	}

	@Remote(id = 28)
	public String stringSupplier() {
		return testValue + "";
	}

	@Remote(id = 29)
	public long mixed(boolean z, byte b, short s, char c, int i, long j, float f, double d) {
		long result = 0;
		if (z)
			++result;
		result += b;
		result += s;
		result += c;
		result += i;
		result += j;
		result += f;
		result += d;
		return result;
	}

	@Remote(id = 30)
	public long mixed2(boolean z, byte b, short s, char c, int i, long j, float f, double d, String _s, boolean[] zarr, byte[] barr, short[] sarr, char[] carr, int[] iarr, long[] jarr, float[] farr, double[] darr, String[] _sarr) {
		long result = 0;
		if (z)
			++result;
		result += b;
		result += s;
		result += c;
		result += i;
		result += j;
		result += f;
		result += d;
		result += _s.length();
		for (boolean a : zarr) {
			if (a)
				++result;
		}
		for (byte a : barr) {
			result += a;
		}
		for (short a : sarr) {
			result += a;
		}
		for (char a : carr) {
			result += a;
		}
		for (int a : iarr) {
			result += a;
		}
		for (long a : jarr) {
			result += a;
		}
		for (float a : farr) {
			result += a;
		}
		for (double a : darr) {
			result += a;
		}
		for (String a : _sarr) {
			result += a.length();
		}

		return result;
	}

	@Remote(id = 31)
	public void throwException() {
		throw new RuntimeException("huhu");
	}

	@Test
	void testRMI() throws Throwable {
		RMISocket ws0 = new RMISocket();
		RMISocket ws1 = new RMISocket();
		ws0.setRMIEndPoint(this);
		ws1.setRMIEndPoint(this);

		RMITestSession session0 = new RMITestSession(ws1);
		RMITestSession session1 = new RMITestSession(ws0);
		session0.open();
		session1.open();
		ws0.onConnect(session0);
		ws1.onConnect(session1);

		ws0.registerServerHandlers(this);
		ws1.registerServerHandlers(this);
		RemoteHandlers handlers0 = ws0.registerClientHandlers(RemoteHandlers.class);
		RemoteHandlers handlers1 = ws1.registerClientHandlers(RemoteHandlers.class);

		Thread thread = new Thread(new Runnable() {
			@Override
			public void run() {
				for (int j = 0; j < 1; ++j) {
					System.out.println(j);
					for (int i = 0; i < 1024; ++i) {
						handlers0.method();
						assertEquals(1, testValue);

						handlers0.consumer((byte) i);
						assertEquals((byte) i, testValue);

						handlers0.consumer(i % 2 == 0);
						handlers0.consumer((short) i);
						assertEquals(i, testValue);

						handlers0.consumer('G');
						assertEquals('G', testValue);

						handlers0.consumer(i);
						assertEquals(i, testValue);

						handlers0.consumer((long) i);
						assertEquals(i, testValue);

						handlers0.consumer((float) i);
						assertEquals(i, testValue);

						handlers0.consumer((double) i);
						assertEquals(i, testValue);

						handlers0.consumer("hello world");
						assertEquals("hello world".length(), testValue);

						handlers0.consumer(new boolean[]{true, false, true});
						assertEquals(2, testValue);

						handlers0.consumer(new byte[]{1, 2, 3});
						assertEquals(6, testValue);

						handlers0.consumer(new short[]{1, 2, 3});
						assertEquals(6, testValue);

						handlers0.consumer(new char[]{'a', 'b', 'c'});
						assertEquals('a' + 'b' + 'c', testValue);

						handlers0.consumer(new int[]{1, 2, 3});
						assertEquals(6, testValue);

						handlers0.consumer(new long[]{1, 2, 3});
						assertEquals(6, testValue);

						handlers0.consumer(new float[]{1, 2, 3});
						assertEquals(6, testValue);

						handlers0.consumer(new double[]{1, 2, 3});
						assertEquals(6, testValue);

						handlers0.consumer(new String[]{"hello", " ", "world"});
						assertEquals(11, testValue);

						boolean value = handlers0.booleanSupplier();
						//System.out.println("value got: "+value);
						byte bvalue = handlers0.byteSupplier();
						//System.out.println("value got: "+bvalue);

						short svalue = handlers0.shortSupplier();
						//System.out.println("value got: "+svalue);

						char cvalue = handlers0.charSupplier();
						//System.out.println("value got: "+cvalue);

						int ivalue = handlers0.intSupplier();
						//System.out.println("value got: "+ivalue);

						long jvalue = handlers0.longSupplier();
						//System.out.println("value got: "+jvalue);

						float fvalue = handlers0.floatSupplier();
						//System.out.println("value got: "+fvalue);

						double dvalue = handlers0.doubleSupplier();
						//System.out.println("value got: "+dvalue);


						String _svalue = handlers0.stringSupplier();
						long sum = handlers0.mixed(true, (byte) 2, (short) 3, (char) 4, 5, 6, 7, 8);
						assertEquals(1 + 2 + 3 + 4 + 5 + 6 + 7 + 8, sum);
						//System.out.println("value got: "+_svalue);

						sum = handlers0.mixed2(true, (byte) 2, (short) 3, (char) 4, 5, 6, 7, 8, "123456789", new boolean[]{true, false}, new byte[]{1, 2}, new short[]{3, 4}, new char[]{5, 6}, new int[]{7, 8}, new long[]{9, 10}, new float[]{11, 12}, new double[]{13, 14}, new String[]{"hello", " ", "world"});
						assertEquals(1 + 2 + 3 + 4 + 5 + 6 + 7 + 8 + 9 + 1 + 1 + 2 + 3 + 4 + 5 + 6 + 7 + 8 + 9 + 10 + 11 + 12 + 13 + 14 + 5 + 1 + 5, sum);
						//System.out.println("value got: "+_svalue);

						sum = handlers0.mixed2(true, (byte) 1, (short) 2, 'a', 4, 5, 6, 7, "world", new boolean[]{true, false}, new byte[]{1, 2, 3, 4}, new short[]{1, 2, 3, 4, 5}, new char[]{'h', 'i'}, new int[]{1, 2, 3, 4, 5}, new long[]{1, 2, 3, 4, 5, 6}, new float[]{1, 2, 3, 4}, new double[]{1, 2, 3, 4, 5}, new String[]{"hello", " ", "world"});
						try {
							handlers0.throwException();
							fail();
						} catch (RemoteException e) {
						}
						//System.out.println("value got: "+_svalue);
									/*
									 *
									 try {
									 Thread.sleep(1000);
									 } catch (InterruptedException e) {
									 e.printStackTrace();
									 }*/
					}
				}
			}
		});
		thread.start();
		thread.join();

		final int threadCount = 8;
		final int iterations = 1024;
		LinkedList<Thread> threads = new LinkedList<>();
		counter.set(0);
		for (int i = 0; i < threadCount; ++i) {
			Thread t = new Thread(new Runnable() {
				public void run() {
					for (int i = 0; i < iterations; ++i) {
						handlers0.method();
						handlers1.method();
						handlers0.consumer((byte) i);
						handlers1.consumer((byte) i);

						handlers0.consumer(i % 2 == 0);
						handlers1.consumer(i % 2 == 0);
						handlers0.consumer((short) i);
						handlers1.consumer((short) i);

						handlers0.consumer('G');
						handlers1.consumer('G');

						handlers0.consumer(i);
						handlers1.consumer(i);

						handlers0.consumer((long) i);
						handlers1.consumer((long) i);

						handlers0.consumer((float) i);
						handlers1.consumer((float) i);

						handlers0.consumer((double) i);
						handlers1.consumer((double) i);

						handlers0.consumer("hello world");
						handlers1.consumer("hello world");

						handlers0.consumer(new boolean[]{true, false, true});
						handlers1.consumer(new boolean[]{true, false, true});

						handlers0.consumer(new byte[]{1, 2, 3});
						handlers1.consumer(new byte[]{1, 2, 3});

						handlers0.consumer(new short[]{1, 2, 3});
						handlers1.consumer(new short[]{1, 2, 3});

						handlers0.consumer(new char[]{'a', 'b', 'c'});
						handlers1.consumer(new char[]{'a', 'b', 'c'});

						handlers0.consumer(new int[]{1, 2, 3});
						handlers1.consumer(new int[]{1, 2, 3});

						handlers0.consumer(new long[]{1, 2, 3});
						handlers1.consumer(new long[]{1, 2, 3});

						handlers0.consumer(new float[]{1, 2, 3});
						handlers1.consumer(new float[]{1, 2, 3});

						handlers0.consumer(new double[]{1, 2, 3});
						handlers1.consumer(new double[]{1, 2, 3});

						handlers0.consumer(new String[]{"hello", " ", "world"});
						handlers1.consumer(new String[]{"hello", " ", "world"});

						boolean value = handlers0.booleanSupplier();
						value = handlers1.booleanSupplier();
						//System.out.println("value got: "+value);
						byte bvalue = handlers0.byteSupplier();
						bvalue = handlers1.byteSupplier();
						//System.out.println("value got: "+bvalue);

						short svalue = handlers0.shortSupplier();
						svalue = handlers1.shortSupplier();
						//System.out.println("value got: "+svalue);

						char cvalue = handlers0.charSupplier();
						cvalue = handlers1.charSupplier();
						//System.out.println("value got: "+cvalue);

						int ivalue = handlers0.intSupplier();
						ivalue = handlers1.intSupplier();
						//System.out.println("value got: "+ivalue);

						long jvalue = handlers0.longSupplier();
						jvalue = handlers1.longSupplier();
						//System.out.println("value got: "+jvalue);

						float fvalue = handlers0.floatSupplier();
						fvalue = handlers1.floatSupplier();
						//System.out.println("value got: "+fvalue);

						double dvalue = handlers0.doubleSupplier();
						dvalue = handlers1.doubleSupplier();
						//System.out.println("value got: "+dvalue);


						String _svalue = handlers0.stringSupplier();
						_svalue = handlers1.stringSupplier();
						long sum = handlers0.mixed(true, (byte) 2, (short) 3, (char) 4, 5, 6, 7, 8);
						sum = handlers1.mixed(true, (byte) 2, (short) 3, (char) 4, 5, 6, 7, 8);
						//System.out.println("value got: "+_svalue);

						sum = handlers0.mixed2(true, (byte) 2, (short) 3, (char) 4, 5, 6, 7, 8, "123456789", new boolean[]{true, false}, new byte[]{1, 2}, new short[]{3, 4}, new char[]{5, 6}, new int[]{7, 8}, new long[]{9, 10}, new float[]{11, 12}, new double[]{13, 14}, new String[]{"hello", " ", "world"});
						sum = handlers1.mixed2(true, (byte) 2, (short) 3, (char) 4, 5, 6, 7, 8, "123456789", new boolean[]{true, false}, new byte[]{1, 2}, new short[]{3, 4}, new char[]{5, 6}, new int[]{7, 8}, new long[]{9, 10}, new float[]{11, 12}, new double[]{13, 14}, new String[]{"hello", " ", "world"});
						//System.out.println("value got: "+_svalue);

						sum = handlers0.mixed2(true, (byte) 1, (short) 2, 'a', 4, 5, 6, 7, "world", new boolean[]{true, false}, new byte[]{1, 2, 3, 4}, new short[]{1, 2, 3, 4, 5}, new char[]{'h', 'i'}, new int[]{1, 2, 3, 4, 5}, new long[]{1, 2, 3, 4, 5, 6}, new float[]{1, 2, 3, 4}, new double[]{1, 2, 3, 4, 5}, new String[]{"hello", " ", "world"});
						sum = handlers1.mixed2(true, (byte) 1, (short) 2, 'a', 4, 5, 6, 7, "world", new boolean[]{true, false}, new byte[]{1, 2, 3, 4}, new short[]{1, 2, 3, 4, 5}, new char[]{'h', 'i'}, new int[]{1, 2, 3, 4, 5}, new long[]{1, 2, 3, 4, 5, 6}, new float[]{1, 2, 3, 4}, new double[]{1, 2, 3, 4, 5}, new String[]{"hello", " ", "world"});
					}
				}
			});
			threads.add(t);
		}
		for (Thread t : threads) {
			t.start();
		}

		for (Thread t : threads) {
			t.join();
		}
		assertEquals(2 * threadCount * iterations, counter.get());
		System.out.println("done");
	}

	@Override
	public void onConnect(RMISocket rmiWebSocket, Map<String, List<String>> params) {
	}

	@Override
	public void onDisconnect(RMISocket rmiWebSocket) {

	}

	interface RemoteHandlers {
		@Remote(id = 1)
		void method();

		@Remote(id = 2)
		void consumer(boolean value);

		@Remote(id = 3)
		void consumer(byte value);

		@Remote(id = 4)
		void consumer(short value);

		@Remote(id = 5)
		void consumer(char value);

		@Remote(id = 6)
		void consumer(int value);

		@Remote(id = 7)
		void consumer(long value);

		@Remote(id = 8)
		void consumer(float value);

		@Remote(id = 9)
		void consumer(double value);

		@Remote(id = 10)
		void consumer(String value);

		@Remote(id = 11)
		void consumer(boolean[] value);

		@Remote(id = 12)
		void consumer(byte[] value);

		@Remote(id = 13)
		void consumer(short[] value);

		@Remote(id = 14)
		void consumer(char[] value);

		@Remote(id = 15)
		void consumer(int[] value);

		@Remote(id = 16)
		void consumer(long[] value);

		@Remote(id = 17)
		void consumer(float[] value);

		@Remote(id = 18)
		void consumer(double[] value);

		@Remote(id = 19)
		void consumer(String[] value);

		@Remote(id = 20)
		boolean booleanSupplier();

		@Remote(id = 21)
		byte byteSupplier();

		@Remote(id = 22)
		short shortSupplier();

		@Remote(id = 23)
		char charSupplier();

		@Remote(id = 24)
		int intSupplier();

		@Remote(id = 25)
		long longSupplier();

		@Remote(id = 26)
		float floatSupplier();

		@Remote(id = 27)
		double doubleSupplier();

		@Remote(id = 28)
		String stringSupplier();

		@Remote(id = 29)
		long mixed(boolean z, byte b, short s, char c, int i, long j, float f, double d);

		@Remote(id = 30)
		long mixed2(boolean z, byte b, short s, char c, int i, long j, float f, double d, String _s, boolean[] zarr, byte[] barr, short[] sarr, char[] carr, int[] iart, long[] jarr, float[] farr, double[] darr, String[] _sarr);

		@Remote(id = 31)
		public void throwException();
	}

}
